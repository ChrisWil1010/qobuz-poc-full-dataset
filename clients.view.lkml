view: clients {
  sql_table_name: PUBLIC.CLIENTS ;;

  dimension: adresse {
    type: string
    sql: ${TABLE}.ADDRESS ;;
  }

#   dimension: audio_player_id { hidden: yes
#     type: string
#     sql: ${TABLE}.AUDIO_PLAYER_ID ;;
#   }

  dimension: birthday {
    type: string
    sql: ${TABLE}.BIRTHDATE ;;
  }

  dimension: age {
    type: number
    sql: datediff(year, ${birthday}, current_date())  ;;
  }

#   dimension: civilite {
#     type: string
#     sql: ${TABLE}.CIVILITY ;;
#   }

#   dimension: code_activation {
#     type: string
#     sql: ${TABLE}.CODE_ACTIVATION ;;
#   }

#   dimension: code_pairing {
#     type: string
#     sql: ${TABLE}.CODE_PAIRING ;;
#   }
#
#   dimension: code_pairing_generated_at {
#     type: string
#     sql: ${TABLE}.CODE_PAIRING_GENERATED_AT ;;
#   }

  dimension: country_code {
    type: string
    sql: ${TABLE}.COUNTRY ;;
  }

  dimension: cp {
    type: string
    sql: ${TABLE}.POSTALCODE ;;
  }

#   dimension: date_creation {
#     type: string
#     sql: ${TABLE}.DATE_CREATION ;;
#     }

# assumed that date_creation is the same as inscription date from the first file to the second file
  dimension_group: creation {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.INSCRIPTIONDATE ;;
  }

#   dimension: date_modif {
#     type: string
#     sql: ${TABLE}.DATE_MODIF ;;
#   }

#   dimension: deleted_at {
#     type: string
#     sql: ${TABLE}.DELETED_AT ;;
#   }

#   dimension: detected_country_code {
#     type: string
#     sql: ${TABLE}.DETECTED_COUNTRY_CODE ;;
#   }
#
#   dimension: detected_language_code {
#     type: string
#     sql: ${TABLE}.DETECTED_LANGUAGE_CODE ;;
#   }

  dimension: email {
    type: string
    sql: ${TABLE}.EMAIL ;;
  }

# civility is gender not "civilité" in the new tables
  dimension: gender {
    type: string
    sql: ${TABLE}.CIVILITY ;;
  }

  dimension: id_client {
    type: string
    sql: ${TABLE}.USERID ;;
  }

  dimension: language_code {
    type: string
    sql: ${TABLE}.LANG ;;
  }

#   dimension: last_signin_at {
#     type: string
#     sql: ${TABLE}.LAST_SIGNIN_AT ;;
#   }

#   dimension: login {
#     type: string
#     sql: ${TABLE}.LOGIN ;;
#   }

#   dimension: mail_verif {
#     type: string
#     sql: ${TABLE}.MAIL_VERIF ;;
#   }

#   dimension: mobile_verified {
#     type: string
#     sql: ${TABLE}.MOBILE_VERIFIED ;;
#   }
#
#   dimension: mot_passe {
#     type: string
#     sql: ${TABLE}.MOT_PASSE ;;
#   }

# Is assumed to be optin for partener newsletters in the second set of samples
  dimension: newsletter_partenaire {
    type: string
    sql: ${TABLE}.OPTIN ;;
  }

  dimension: nom {
    type: string
    sql: ${TABLE}.LASTNAME ;;
  }

#   dimension: override_tryandbuy_limitation {
#     type: number
#     sql: ${TABLE}.OVERRIDE_TRYANDBUY_LIMITATION ;;
#   }

  dimension: pays {
    type: number
    sql: ${TABLE}.COUNTRY ;;
  }

#   dimension: photo {
#     type: string
#     sql: ${TABLE}.PHOTO ;;
#   }

#   dimension: player_settings {
#     type: string
#     sql: ${TABLE}.PLAYER_SETTINGS ;;
#   }

#   dimension: preferred_payment {
#     type: string
#     sql: ${TABLE}.PREFERRED_PAYMENT ;;
#   }

  dimension: prenom {
    type: string
    sql: ${TABLE}.FIRSTNAME ;;
  }

#   dimension: prenom2 {
#     type: string
#     sql: ${TABLE}.PRENOM2 ;;
#   }

#   dimension: public_id { hidden: yes
#     type: string
#     sql: ${TABLE}.PUBLIC_ID ;;
#   }

#   dimension: qb_features {
#     type: string
#     sql: ${TABLE}.QB_FEATURES ;;
#   }
#
#   dimension: send_prompt_for_reaction {
#     type: number
#     sql: ${TABLE}.SEND_PROMPT_FOR_REACTION ;;
#   }
#
#   dimension: send_purchase_summary {
#     type: string
#     sql: ${TABLE}.SEND_PURCHASE_SUMMARY ;;
#   }

#   dimension: sid {
#     type: string
#     sql: ${TABLE}.SID ;;
#   }
#
#   dimension: signup_ip {
#     type: string
#     sql: ${TABLE}.SIGNUP_IP ;;
#   }

#   dimension: signup_partner_id { hidden: yes
#     type: string
#     sql: ${TABLE}.SIGNUP_PARTNER_ID ;;
#   }
#
#   dimension: signup_referer {
#     type: string
#     sql: ${TABLE}.SIGNUP_REFERER ;;
#   }
#
#   dimension: signup_user_agent {
#     type: string
#     sql: ${TABLE}.SIGNUP_USER_AGENT ;;
#   }

#   dimension: sponsor_id { hidden: yes
#     type: string
#     sql: ${TABLE}.SPONSOR_ID ;;
#   }
#
#   dimension: sponsored_count {
#     type: number
#     sql: ${TABLE}.SPONSORED_COUNT ;;
#   }
#
#   dimension: streaming_endpoint {
#     type: number
#     sql: ${TABLE}.STREAMING_ENDPOINT ;;
#   }

  dimension: tel {
    type: string
    sql: ${TABLE}.TELEPHONENUMBER1 ;;
  }

  dimension: tel2 {
    type: string
    sql: ${TABLE}.TELEPHONENUMBER2 ;;
  }

  dimension: ville {
    type: string
    sql: ${TABLE}.CITY ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }

  measure: distinct_customers {
    type: count_distinct
    sql: ${id_client} ;;
  }
}
