view: sales_download {
  sql_table_name: PUBLIC.SALES_DOWNLOAD ;;

  dimension: album_artist_name {
    type: string
    sql: ${TABLE}.ALBUMARTISTNAME ;;
  }

  dimension: album_artist_slug {
    type: string
    sql: ${TABLE}.ALBUMARTISTSLUG ;;
  }

  dimension: album_grid {
    type: string
    sql: ${TABLE}.ALBUMGRID ;;
  }

  dimension: album_id {
    hidden: no
    type: string
    sql: ${TABLE}.ALBUMID ;;
  }

  dimension: album_title {
    type: string
    sql: ${TABLE}.ALBUMTITLE ;;
  }

  dimension: article_type {
    type: string
    sql: ${TABLE}.ARTICLETYPE ;;
  }

  dimension: deal_id {
    hidden: no
    type: string
    sql: ${TABLE}.DEALID ;;
  }

  dimension: deal_type {
    type: string
    sql: ${TABLE}.DEALTYPE ;;
  }

  dimension: delivery_date {
    type: string
    sql: ${TABLE}.DELIVERYDATE ;;
  }

  dimension: duration {
    type: number
    sql: ${TABLE}.DURATION ;;
  }

  dimension: file_period {
    type: string
    sql: ${TABLE}.FILEPERIOD ;;
  }

  dimension: file_type {
    type: string
    sql: ${TABLE}.FILETYPE ;;
  }

  dimension: file_version {
    type: string
    sql: ${TABLE}.FILEVERSION ;;
  }

  dimension: genre {
    type: string
    sql: ${TABLE}.GENRE ;;
  }

  dimension: genre_path {
    type: string
    sql: ${TABLE}.GENREPATH ;;
  }

  dimension: isrc {
    type: string
    sql: ${TABLE}.ISRC ;;
  }

  dimension: label {
    type: string
    sql: ${TABLE}.LABEL ;;
  }

  dimension: labelid {
    type: string
    sql: ${TABLE}.LABELID ;;
  }

  dimension: label_slug {
    type: string
    sql: ${TABLE}.LABELSLUG ;;
  }

  dimension: number_of_supports {
    type: string
    sql: ${TABLE}.NUMBEROFSUPPORTS ;;
  }

  dimension: number_of_tracks {
    type: number
    sql: ${TABLE}.NUMBEROFTRACKS ;;
  }

  dimension: order_id {
    hidden: no
    type: string
    sql: ${TABLE}.ORDERID ;;
  }

  dimension: payment_method {
    type: string
    sql: ${TABLE}.PAYMENTMETHOD ;;
  }

  dimension: price_code {
    type: string
    sql: ${TABLE}.PRICECODE ;;
  }

  dimension: purchase_date {
    type: string
    sql: ${TABLE}.PURCHASEDATE ;;
  }

  dimension_group: purchase_date_as_timestamp {
    type: time
    timeframes: [date, year, month, week_of_year, hour, minute]
    sql: TO_TIMESTAMP(${TABLE}.PURCHASEDATE, 'yyyy/mm/dd hh24:mi:ss') ;;
    datatype: date
  }

  dimension: quality {
    type: string
    sql: ${TABLE}.QUALITY ;;
  }

  dimension: quantity {
    type: number
    sql: ${TABLE}.QUANTITY ;;
  }

  dimension: retail_currency {
    type: string
    sql: ${TABLE}.RETAILCURRENCY ;;
  }

  dimension: retail_currency_rate {
    type: number
    sql: ${TABLE}.RETAILCURRENCYRATE ;;
  }

  dimension: retail_price_excl_vat {
    type: number
    sql: ${TABLE}.RETAILPRICEEXCLVAT ;;
  }

  dimension: retail_price_excl_vat_eur {
    type: number
    sql: ${TABLE}.RETAILPRICEEXCLVATEUR ;;
  }

  dimension: retail_price_incl_vat {
    type: number
    sql: ${TABLE}.RETAILPRICEINCLVAT ;;
  }

  dimension: retail_price_incl_vat_eur {
    type: number
    sql: ${TABLE}.RETAILPRICEINCLVATEUR ;;
  }

  dimension: retail_vat_amount {
    type: number
    sql: ${TABLE}.RETAILVATAMOUNT ;;
  }

  dimension: retail_vat_amount_eur {
    type: number
    sql: ${TABLE}.RETAILVATAMOUNTEUR ;;
  }

  dimension: retail_vat_rate {
    type: number
    sql: ${TABLE}.RETAILVATRATE ;;
  }

  dimension: royalty_amount {
    type: number
    sql: ${TABLE}.ROYALTYAMOUNT ;;
  }

  dimension: royalty_amount_eur {
    type: number
    sql: ${TABLE}.ROYALTYAMOUNTEUR ;;
  }

  dimension: royalty_currency {
    type: string
    sql: ${TABLE}.ROYALTYCURRENCY ;;
  }

  dimension: royalty_currency_rate {
    type: number
    sql: ${TABLE}.ROYALTYCURRENCYRATE ;;
  }

  dimension: royalty_per_unit {
    type: number
    sql: ${TABLE}.ROYALTYPERUNIT ;;
  }

  dimension: royalty_per_unit_eur {
    type: number
    sql: ${TABLE}.ROYALTYPERUNITEUR ;;
  }

  dimension: sales_type {
    type: string
    sql: ${TABLE}.SALESTYPE ;;
  }

  dimension: supplier {
    type: string
    sql: ${TABLE}.SUPPLIER ;;
  }

  dimension: supplier_id { hidden: yes
    type: string
    sql: ${TABLE}.SUPPLIERID ;;
  }

  dimension: support_number {
    type: number
    sql: ${TABLE}.SUPPORTNUMBER ;;
  }

  dimension: track_artist_name {
    type: string
    sql: ${TABLE}.TRACKARTISTNAME ;;
  }

  dimension: track_artist_slug {
    type: string
    sql: ${TABLE}.TRACKARTISTSLUG ;;
  }

  dimension: track_grid {
    type: string
    sql: ${TABLE}.TRACKGRID ;;
  }

  dimension: track_id {
    hidden: no
    type: string
    sql: ${TABLE}.TRACKID ;;
  }

  dimension: track_number {
    type: number
    sql: ${TABLE}.TRACKNUMBER ;;
  }

  dimension: track_title {
    type: string
    sql: ${TABLE}.TRACKTITLE ;;
  }

  dimension: try_and_buy_status {
    type: string
    sql: ${TABLE}.TRYANDBUYSTATUS ;;
  }

  dimension: upc {
    type: string
    sql: ${TABLE}.UPC ;;
  }

  dimension: user_id {
    hidden: no
    type: string
    sql: ${TABLE}.USERID ;;
  }

  dimension: user_subscription_offer {
    type: string
    sql: ${TABLE}.USERSUBSCRIPTIONOFFER ;;
  }

  dimension: user_subscription_periodicity {
    type: string
    sql: ${TABLE}.USERSUBSCRIPTIONPERIODICITY ;;
  }

  dimension: user_subscription_slug {
    type: string
    sql: ${TABLE}.USERSUBSCRIPTIONSLUG ;;
  }

  dimension: user_zone {
    type: string
    sql: ${TABLE}.USERZONE ;;
  }

  measure: count {
    type: count
    drill_fields: [album_artist_name, track_artist_name]
  }

  measure: amount_HT {
    type: sum
    label: "Sales Amount €"
    sql: ${retail_price_excl_vat_eur}  ;;
  }

  measure: royalty_amount_sum {
    type: sum
    label:  "Royalty Sum €"
    sql: ${royalty_amount_eur}  ;;
  }

  measure: revenue {
    type: sum
    sql: ${retail_price_excl_vat}-${royalty_amount_eur}  ;;
  }

  measure: number_of_sales {
    type: count
    drill_fields: [order_id]
  }

  measure: distinct_customers{
    type: count_distinct
    sql: ${user_id}  ;;
  }

}
