view: sales_global {
  #sql_table_name: PUBLIC.SALES_GLOBAL ;;
  # a derived table added to be able to use granularity filter
  derived_table: {
    sql:
    SELECT *
    FROM PUBLIC.SALES_GLOBAL
    WHERE
    CASE
    WHEN {% parameter date_granularity %} = 'Past Week' THEN
    ( ORDERDATE  >= TO_DATE(DATEADD('day', -6, CURRENT_DATE() )) ) AND ( ORDERDATE < TO_DATE(DATEADD('day', 7, DATEADD('day', -6, CURRENT_DATE() ))) )
    WHEN {% parameter date_granularity %} = 'Past Month' THEN
    ( ORDERDATE  >= TO_DATE(DATEADD('day', -29, CURRENT_DATE() )) ) AND ( ORDERDATE < TO_DATE(DATEADD('day', 30, DATEADD('day', -29, CURRENT_DATE() ))) )
    WHEN {% parameter date_granularity %} = 'Past Quarter' THEN
    ( ORDERDATE  >= TO_DATE(DATEADD('day', -89, CURRENT_DATE() )) ) AND ( ORDERDATE < TO_DATE(DATEADD('day', 90, DATEADD('day', -89, CURRENT_DATE() ))) )
    WHEN {% parameter date_granularity %} = 'Past Year' THEN
    ( ORDERDATE  >= TO_DATE(DATEADD('day', -364, CURRENT_DATE() )) ) AND ( ORDERDATE < TO_DATE(DATEADD('day', 365, DATEADD('day', -364, CURRENT_DATE() ))) )
    ELSE NULL
    END
    ;;

    }

    dimension: card_country {
      type: string
      sql: ${TABLE}.CARDCOUNTRY ;;
    }

    dimension: currency {
      type: string
      sql: ${TABLE}.CURRENCY ;;
    }

    dimension: discount {
      type: number
      sql: ${TABLE}.DISCOUNT ;;
    }

    dimension: gross_amount {
      type: number
      sql: ${TABLE}.GROSSAMOUNT ;;
    }

    dimension: order_date_plain {
      type: string
      #primary_key: yes
      sql: ${TABLE}.ORDERDATE ;;
    }

    dimension_group: order {
      label:  "Order Date"
      type: time
#     timeframes: [
#       raw,
#       date,
#       week,
#       month,
#       quarter,
#       year
#     ]
      convert_tz: no
      datatype: date
      sql: ${TABLE}.ORDERDATE ;;

#     sql:
#     CASE
#     WHEN {% parameter date_granularity %} = 'Past Week' THEN
#     ( ${TABLE}.ORDERDATE  >= TO_DATE(DATEADD('day', -6, CURRENT_DATE() )) ) AND ( ${TABLE}.ORDERDATE < TO_DATE(DATEADD('day', 7, DATEADD('day', -6, CURRENT_DATE() ))) )
#     WHEN {% parameter date_granularity %} = 'Past Month' THEN
#     ( ${TABLE}.ORDERDATE  >= TO_DATE(DATEADD('day', -29, CURRENT_DATE() )) ) AND ( ${TABLE}.ORDERDATE < TO_DATE(DATEADD('day', 30, DATEADD('day', -29, CURRENT_DATE() ))) )
#   WHEN {% parameter date_granularity %} = 'Past Quarter' THEN
#     ( ${TABLE}.ORDERDATE  >= TO_DATE(DATEADD('day', -89, CURRENT_DATE() )) ) AND ( ${TABLE}.ORDERDATE < TO_DATE(DATEADD('day', 90, DATEADD('day', -89, CURRENT_DATE() ))) )
#   WHEN {% parameter date_granularity %} = 'Past Year' THEN
#     ( ${TABLE}.ORDERDATE  >= TO_DATE(DATEADD('day', -364, CURRENT_DATE() )) ) AND ( ${TABLE}.ORDERDATE < TO_DATE(DATEADD('day', 365, DATEADD('day', -364, CURRENT_DATE() ))) )
#   ELSE NULL
#     END
#     ;;

      #sql:  DATEADD(DAY, 65, ${TABLE}.ORDERDATE) ;;
      # that's a fake step for making dates have more convenient values to slice for current/previous weeks and months
    }

#   # added to have a parametrized filter on UI
#   dimension_group: orderdate {
#     type: time
#     timeframes: [
#       raw,
#       date,
#       week,
#       month,
#       quarter,
#       year
#     ]
#     convert_tz: no
#     datatype: date
#     #label_from_parameter: date_granularity
#     sql:
#        CASE
#          WHEN {% parameter date_granularity %} = 'Past Week' THEN
#            (((DATEADD(DAY, 65, sales_global.ORDERDATE) ) >= ((TO_DATE(DATEADD('day', -6, CURRENT_DATE())))) AND (DATEADD(DAY, 65, sales_global.ORDERDATE) ) < ((TO_DATE(DATEADD('day', 7, DATEADD('day', -6, CURRENT_DATE())))))))
#          WHEN {% parameter date_granularity %} = 'Past Month' THEN
#             (((DATEADD(DAY, 65, sales_global.ORDERDATE) ) >= ((TO_DATE(DATE_TRUNC('month', CURRENT_DATE())))) AND (DATEADD(DAY, 65, sales_global.ORDERDATE) ) < ((TO_DATE(DATEADD('month', 1, DATE_TRUNC('month', CURRENT_DATE())))))))
#          WHEN {% parameter date_granularity %} = 'Past Quarter' THEN
#            (((DATEADD(DAY, 65, sales_global.ORDERDATE) ) >= ((TO_DATE(CAST(DATE_TRUNC('quarter', CURRENT_DATE()) AS DATE)))) AND (DATEADD(DAY, 65, sales_global.ORDERDATE) ) < ((TO_DATE(DATEADD('month', 3, CAST(DATE_TRUNC('quarter', CAST(DATE_TRUNC('quarter', CURRENT_DATE()) AS DATE)) AS DATE)))))))
#          WHEN {% parameter date_granularity %} = 'Past Year' THEN
#            (((DATEADD(DAY, 65, sales_global.ORDERDATE) ) >= ((TO_DATE(DATE_TRUNC('year', CURRENT_DATE())))) AND (DATEADD(DAY, 65, sales_global.ORDERDATE) ) < ((TO_DATE(DATEADD('year', 1, DATE_TRUNC('year', CURRENT_DATE())))))))
#          ELSE
#            NULL
#        END ;;
#   }

    # experimenting with "N-1"
#   dimension: is_before_mtd {
#     type: yesno
#     sql:
#     datediff(month, ${orderdate_date}, CURRENT_TIMESTAMP) >= 1 and  datediff(day, ${orderdate_date}, CURRENT_TIMESTAMP) < 2  ;;
#     }
#
    #     (EXTRACT(DAY FROM ${orderdate}) < EXTRACT(DAY FROM CURRENT_TIMESTAMP)
#       OR
#       (
#         EXTRACT(DAY FROM ${orderdate}) = EXTRACT(DAY FROM CURRENT_TIMESTAMP) AND
#         EXTRACT(HOUR FROM ${orderdate}) < EXTRACT(HOUR FROM CURRENT_TIMESTAMP)
#       )
#       OR
#       (
#         EXTRACT(DAY FROM ${orderdate}) = EXTRACT(DAY FROM CURRENT_TIMESTAMP) AND
#         EXTRACT(HOUR FROM ${orderdate}) <= EXTRACT(HOUR FROM CURRENT_TIMESTAMP) AND
#         EXTRACT(MINUTE FROM ${orderdate}) < EXTRACT(MINUTE FROM CURRENT_TIMESTAMP)
#       )
#     )

    dimension: order_id {
      hidden: no
      type: string
      sql: ${TABLE}.ORDERID ;;
    }

    dimension: order_ip_country {
      type: string
      sql: ${TABLE}.ORDERIPCOUNTRY ;;
    }

#   dimension: orderzone {
#     type: string
#     sql: ${TABLE}.ORDERZONE ;;
#   }

# modified to have correct countries mapping
    dimension: order_zone {
      type: string
      sql:
      CASE
        WHEN ${TABLE}.ORDERZONE = 'AF' THEN 'AFG'
        WHEN ${TABLE}.ORDERZONE = 'AX' THEN 'ALA'
        WHEN ${TABLE}.ORDERZONE = 'AL' THEN 'ALB'
        WHEN ${TABLE}.ORDERZONE = 'DZ' THEN 'DZA'
        WHEN ${TABLE}.ORDERZONE = 'AS' THEN 'ASM'
        WHEN ${TABLE}.ORDERZONE = 'AD' THEN 'AND'
        WHEN ${TABLE}.ORDERZONE = 'AO' THEN 'AGO'
        WHEN ${TABLE}.ORDERZONE = 'AI' THEN 'AIA'
        WHEN ${TABLE}.ORDERZONE = 'AQ' THEN 'ATA'
        WHEN ${TABLE}.ORDERZONE = 'AG' THEN 'ATG'
        WHEN ${TABLE}.ORDERZONE = 'AR' THEN 'ARG'
        WHEN ${TABLE}.ORDERZONE = 'AM' THEN 'ARM'
        WHEN ${TABLE}.ORDERZONE = 'AW' THEN 'ABW'
        WHEN ${TABLE}.ORDERZONE = 'AU' THEN 'AUS'
        WHEN ${TABLE}.ORDERZONE = 'AT' THEN 'AUT'
        WHEN ${TABLE}.ORDERZONE = 'AZ' THEN 'AZE'
        WHEN ${TABLE}.ORDERZONE = 'BS' THEN 'BHS'
        WHEN ${TABLE}.ORDERZONE = 'BH' THEN 'BHR'
        WHEN ${TABLE}.ORDERZONE = 'BD' THEN 'BGD'
        WHEN ${TABLE}.ORDERZONE = 'BB' THEN 'BRB'
        WHEN ${TABLE}.ORDERZONE = 'BY' THEN 'BLR'
        WHEN ${TABLE}.ORDERZONE = 'BE' THEN 'BEL'
        WHEN ${TABLE}.ORDERZONE = 'BZ' THEN 'BLZ'
        WHEN ${TABLE}.ORDERZONE = 'BJ' THEN 'BEN'
        WHEN ${TABLE}.ORDERZONE = 'BM' THEN 'BMU'
        WHEN ${TABLE}.ORDERZONE = 'BT' THEN 'BTN'
        WHEN ${TABLE}.ORDERZONE = 'BO' THEN 'BOL'
        WHEN ${TABLE}.ORDERZONE = 'BQ' THEN 'BES'
        WHEN ${TABLE}.ORDERZONE = 'BA' THEN 'BIH'
        WHEN ${TABLE}.ORDERZONE = 'BW' THEN 'BWA'
        WHEN ${TABLE}.ORDERZONE = 'BV' THEN 'BVT'
        WHEN ${TABLE}.ORDERZONE = 'BR' THEN 'BRA'
        WHEN ${TABLE}.ORDERZONE = 'IO' THEN 'IOT'
        WHEN ${TABLE}.ORDERZONE = 'BN' THEN 'BRN'
        WHEN ${TABLE}.ORDERZONE = 'BG' THEN 'BGR'
        WHEN ${TABLE}.ORDERZONE = 'BF' THEN 'BFA'
        WHEN ${TABLE}.ORDERZONE = 'BI' THEN 'BDI'
        WHEN ${TABLE}.ORDERZONE = 'KH' THEN 'KHM'
        WHEN ${TABLE}.ORDERZONE = 'CM' THEN 'CMR'
        WHEN ${TABLE}.ORDERZONE = 'CA' THEN 'CAN'
        WHEN ${TABLE}.ORDERZONE = 'CV' THEN 'CPV'
        WHEN ${TABLE}.ORDERZONE = 'KY' THEN 'CYM'
        WHEN ${TABLE}.ORDERZONE = 'CF' THEN 'CAF'
        WHEN ${TABLE}.ORDERZONE = 'TD' THEN 'TCD'
        WHEN ${TABLE}.ORDERZONE = 'CL' THEN 'CHL'
        WHEN ${TABLE}.ORDERZONE = 'CN' THEN 'CHN'
        WHEN ${TABLE}.ORDERZONE = 'CX' THEN 'CXR'
        WHEN ${TABLE}.ORDERZONE = 'CC' THEN 'CCK'
        WHEN ${TABLE}.ORDERZONE = 'CO' THEN 'COL'
        WHEN ${TABLE}.ORDERZONE = 'KM' THEN 'COM'
        WHEN ${TABLE}.ORDERZONE = 'CG' THEN 'COG'
        WHEN ${TABLE}.ORDERZONE = 'CD' THEN 'COD'
        WHEN ${TABLE}.ORDERZONE = 'CK' THEN 'COK'
        WHEN ${TABLE}.ORDERZONE = 'CR' THEN 'CRI'
        WHEN ${TABLE}.ORDERZONE = 'CI' THEN 'CIV'
        WHEN ${TABLE}.ORDERZONE = 'HR' THEN 'HRV'
        WHEN ${TABLE}.ORDERZONE = 'CU' THEN 'CUB'
        WHEN ${TABLE}.ORDERZONE = 'CW' THEN 'CUW'
        WHEN ${TABLE}.ORDERZONE = 'CY' THEN 'CYP'
        WHEN ${TABLE}.ORDERZONE = 'CZ' THEN 'CZE'
        WHEN ${TABLE}.ORDERZONE = 'DK' THEN 'DNK'
        WHEN ${TABLE}.ORDERZONE = 'DJ' THEN 'DJI'
        WHEN ${TABLE}.ORDERZONE = 'DM' THEN 'DMA'
        WHEN ${TABLE}.ORDERZONE = 'DO' THEN 'DOM'
        WHEN ${TABLE}.ORDERZONE = 'EC' THEN 'ECU'
        WHEN ${TABLE}.ORDERZONE = 'EG' THEN 'EGY'
        WHEN ${TABLE}.ORDERZONE = 'SV' THEN 'SLV'
        WHEN ${TABLE}.ORDERZONE = 'GQ' THEN 'GNQ'
        WHEN ${TABLE}.ORDERZONE = 'ER' THEN 'ERI'
        WHEN ${TABLE}.ORDERZONE = 'EE' THEN 'EST'
        WHEN ${TABLE}.ORDERZONE = 'ET' THEN 'ETH'
        WHEN ${TABLE}.ORDERZONE = 'FK' THEN 'FLK'
        WHEN ${TABLE}.ORDERZONE = 'FO' THEN 'FRO'
        WHEN ${TABLE}.ORDERZONE = 'FJ' THEN 'FJI'
        WHEN ${TABLE}.ORDERZONE = 'FI' THEN 'FIN'
        WHEN ${TABLE}.ORDERZONE = 'FR' THEN 'FRA'
        WHEN ${TABLE}.ORDERZONE = 'GF' THEN 'GUF'
        WHEN ${TABLE}.ORDERZONE = 'PF' THEN 'PYF'
        WHEN ${TABLE}.ORDERZONE = 'TF' THEN 'ATF'
        WHEN ${TABLE}.ORDERZONE = 'GA' THEN 'GAB'
        WHEN ${TABLE}.ORDERZONE = 'GM' THEN 'GMB'
        WHEN ${TABLE}.ORDERZONE = 'GE' THEN 'GEO'
        WHEN ${TABLE}.ORDERZONE = 'DE' THEN 'DEU'
        WHEN ${TABLE}.ORDERZONE = 'GH' THEN 'GHA'
        WHEN ${TABLE}.ORDERZONE = 'GI' THEN 'GIB'
        WHEN ${TABLE}.ORDERZONE = 'GR' THEN 'GRC'
        WHEN ${TABLE}.ORDERZONE = 'GL' THEN 'GRL'
        WHEN ${TABLE}.ORDERZONE = 'GD' THEN 'GRD'
        WHEN ${TABLE}.ORDERZONE = 'GP' THEN 'GLP'
        WHEN ${TABLE}.ORDERZONE = 'GU' THEN 'GUM'
        WHEN ${TABLE}.ORDERZONE = 'GT' THEN 'GTM'
        WHEN ${TABLE}.ORDERZONE = 'GG' THEN 'GGY'
        WHEN ${TABLE}.ORDERZONE = 'GN' THEN 'GIN'
        WHEN ${TABLE}.ORDERZONE = 'GW' THEN 'GNB'
        WHEN ${TABLE}.ORDERZONE = 'GY' THEN 'GUY'
        WHEN ${TABLE}.ORDERZONE = 'HT' THEN 'HTI'
        WHEN ${TABLE}.ORDERZONE = 'HM' THEN 'HMD'
        WHEN ${TABLE}.ORDERZONE = 'VA' THEN 'VAT'
        WHEN ${TABLE}.ORDERZONE = 'HN' THEN 'HND'
        WHEN ${TABLE}.ORDERZONE = 'HK' THEN 'HKG'
        WHEN ${TABLE}.ORDERZONE = 'HU' THEN 'HUN'
        WHEN ${TABLE}.ORDERZONE = 'IS' THEN 'ISL'
        WHEN ${TABLE}.ORDERZONE = 'IN' THEN 'IND'
        WHEN ${TABLE}.ORDERZONE = 'ID' THEN 'IDN'
        WHEN ${TABLE}.ORDERZONE = 'IR' THEN 'IRN'
        WHEN ${TABLE}.ORDERZONE = 'IQ' THEN 'IRQ'
        WHEN ${TABLE}.ORDERZONE = 'IE' THEN 'IRL'
        WHEN ${TABLE}.ORDERZONE = 'IM' THEN 'IMN'
        WHEN ${TABLE}.ORDERZONE = 'IL' THEN 'ISR'
        WHEN ${TABLE}.ORDERZONE = 'IT' THEN 'ITA'
        WHEN ${TABLE}.ORDERZONE = 'JM' THEN 'JAM'
        WHEN ${TABLE}.ORDERZONE = 'JP' THEN 'JPN'
        WHEN ${TABLE}.ORDERZONE = 'JE' THEN 'JEY'
        WHEN ${TABLE}.ORDERZONE = 'JO' THEN 'JOR'
        WHEN ${TABLE}.ORDERZONE = 'KZ' THEN 'KAZ'
        WHEN ${TABLE}.ORDERZONE = 'KE' THEN 'KEN'
        WHEN ${TABLE}.ORDERZONE = 'KI' THEN 'KIR'
        WHEN ${TABLE}.ORDERZONE = 'KP' THEN 'PRK'
        WHEN ${TABLE}.ORDERZONE = 'KR' THEN 'KOR'
        WHEN ${TABLE}.ORDERZONE = 'KW' THEN 'KWT'
        WHEN ${TABLE}.ORDERZONE = 'KG' THEN 'KGZ'
        WHEN ${TABLE}.ORDERZONE = 'LA' THEN 'LAO'
        WHEN ${TABLE}.ORDERZONE = 'LV' THEN 'LVA'
        WHEN ${TABLE}.ORDERZONE = 'LB' THEN 'LBN'
        WHEN ${TABLE}.ORDERZONE = 'LS' THEN 'LSO'
        WHEN ${TABLE}.ORDERZONE = 'LR' THEN 'LBR'
        WHEN ${TABLE}.ORDERZONE = 'LY' THEN 'LBY'
        WHEN ${TABLE}.ORDERZONE = 'LI' THEN 'LIE'
        WHEN ${TABLE}.ORDERZONE = 'LT' THEN 'LTU'
        WHEN ${TABLE}.ORDERZONE = 'LU' THEN 'LUX'
        WHEN ${TABLE}.ORDERZONE = 'MO' THEN 'MAC'
        WHEN ${TABLE}.ORDERZONE = 'MK' THEN 'MKD'
        WHEN ${TABLE}.ORDERZONE = 'MG' THEN 'MDG'
        WHEN ${TABLE}.ORDERZONE = 'MW' THEN 'MWI'
        WHEN ${TABLE}.ORDERZONE = 'MY' THEN 'MYS'
        WHEN ${TABLE}.ORDERZONE = 'MV' THEN 'MDV'
        WHEN ${TABLE}.ORDERZONE = 'ML' THEN 'MLI'
        WHEN ${TABLE}.ORDERZONE = 'MT' THEN 'MLT'
        WHEN ${TABLE}.ORDERZONE = 'MH' THEN 'MHL'
        WHEN ${TABLE}.ORDERZONE = 'MQ' THEN 'MTQ'
        WHEN ${TABLE}.ORDERZONE = 'MR' THEN 'MRT'
        WHEN ${TABLE}.ORDERZONE = 'MU' THEN 'MUS'
        WHEN ${TABLE}.ORDERZONE = 'YT' THEN 'MYT'
        WHEN ${TABLE}.ORDERZONE = 'MX' THEN 'MEX'
        WHEN ${TABLE}.ORDERZONE = 'FM' THEN 'FSM'
        WHEN ${TABLE}.ORDERZONE = 'MD' THEN 'MDA'
        WHEN ${TABLE}.ORDERZONE = 'MC' THEN 'MCO'
        WHEN ${TABLE}.ORDERZONE = 'MN' THEN 'MNG'
        WHEN ${TABLE}.ORDERZONE = 'ME' THEN 'MNE'
        WHEN ${TABLE}.ORDERZONE = 'MS' THEN 'MSR'
        WHEN ${TABLE}.ORDERZONE = 'MA' THEN 'MAR'
        WHEN ${TABLE}.ORDERZONE = 'MZ' THEN 'MOZ'
        WHEN ${TABLE}.ORDERZONE = 'MM' THEN 'MMR'
        WHEN ${TABLE}.ORDERZONE = 'NA' THEN 'NAM'
        WHEN ${TABLE}.ORDERZONE = 'NR' THEN 'NRU'
        WHEN ${TABLE}.ORDERZONE = 'NP' THEN 'NPL'
        WHEN ${TABLE}.ORDERZONE = 'NL' THEN 'NLD'
        WHEN ${TABLE}.ORDERZONE = 'NC' THEN 'NCL'
        WHEN ${TABLE}.ORDERZONE = 'NZ' THEN 'NZL'
        WHEN ${TABLE}.ORDERZONE = 'NI' THEN 'NIC'
        WHEN ${TABLE}.ORDERZONE = 'NE' THEN 'NER'
        WHEN ${TABLE}.ORDERZONE = 'NG' THEN 'NGA'
        WHEN ${TABLE}.ORDERZONE = 'NU' THEN 'NIU'
        WHEN ${TABLE}.ORDERZONE = 'NF' THEN 'NFK'
        WHEN ${TABLE}.ORDERZONE = 'MP' THEN 'MNP'
        WHEN ${TABLE}.ORDERZONE = 'NO' THEN 'NOR'
        WHEN ${TABLE}.ORDERZONE = 'OM' THEN 'OMN'
        WHEN ${TABLE}.ORDERZONE = 'PK' THEN 'PAK'
        WHEN ${TABLE}.ORDERZONE = 'PW' THEN 'PLW'
        WHEN ${TABLE}.ORDERZONE = 'PS' THEN 'PSE'
        WHEN ${TABLE}.ORDERZONE = 'PA' THEN 'PAN'
        WHEN ${TABLE}.ORDERZONE = 'PG' THEN 'PNG'
        WHEN ${TABLE}.ORDERZONE = 'PY' THEN 'PRY'
        WHEN ${TABLE}.ORDERZONE = 'PE' THEN 'PER'
        WHEN ${TABLE}.ORDERZONE = 'PH' THEN 'PHL'
        WHEN ${TABLE}.ORDERZONE = 'PN' THEN 'PCN'
        WHEN ${TABLE}.ORDERZONE = 'PL' THEN 'POL'
        WHEN ${TABLE}.ORDERZONE = 'PT' THEN 'PRT'
        WHEN ${TABLE}.ORDERZONE = 'PR' THEN 'PRI'
        WHEN ${TABLE}.ORDERZONE = 'QA' THEN 'QAT'
        WHEN ${TABLE}.ORDERZONE = 'RE' THEN 'REU'
        WHEN ${TABLE}.ORDERZONE = 'RO' THEN 'ROU'
        WHEN ${TABLE}.ORDERZONE = 'RU' THEN 'RUS'
        WHEN ${TABLE}.ORDERZONE = 'RW' THEN 'RWA'
        WHEN ${TABLE}.ORDERZONE = 'BL' THEN 'BLM'
        WHEN ${TABLE}.ORDERZONE = 'SH' THEN 'SHN'
        WHEN ${TABLE}.ORDERZONE = 'KN' THEN 'KNA'
        WHEN ${TABLE}.ORDERZONE = 'LC' THEN 'LCA'
        WHEN ${TABLE}.ORDERZONE = 'MF' THEN 'MAF'
        WHEN ${TABLE}.ORDERZONE = 'PM' THEN 'SPM'
        WHEN ${TABLE}.ORDERZONE = 'VC' THEN 'VCT'
        WHEN ${TABLE}.ORDERZONE = 'WS' THEN 'WSM'
        WHEN ${TABLE}.ORDERZONE = 'SM' THEN 'SMR'
        WHEN ${TABLE}.ORDERZONE = 'ST' THEN 'STP'
        WHEN ${TABLE}.ORDERZONE = 'SA' THEN 'SAU'
        WHEN ${TABLE}.ORDERZONE = 'SN' THEN 'SEN'
        WHEN ${TABLE}.ORDERZONE = 'RS' THEN 'SRB'
        WHEN ${TABLE}.ORDERZONE = 'SC' THEN 'SYC'
        WHEN ${TABLE}.ORDERZONE = 'SL' THEN 'SLE'
        WHEN ${TABLE}.ORDERZONE = 'SG' THEN 'SGP'
        WHEN ${TABLE}.ORDERZONE = 'SX' THEN 'SXM'
        WHEN ${TABLE}.ORDERZONE = 'SK' THEN 'SVK'
        WHEN ${TABLE}.ORDERZONE = 'SI' THEN 'SVN'
        WHEN ${TABLE}.ORDERZONE = 'SB' THEN 'SLB'
        WHEN ${TABLE}.ORDERZONE = 'SO' THEN 'SOM'
        WHEN ${TABLE}.ORDERZONE = 'ZA' THEN 'ZAF'
        WHEN ${TABLE}.ORDERZONE = 'GS' THEN 'SGS'
        WHEN ${TABLE}.ORDERZONE = 'SS' THEN 'SSD'
        WHEN ${TABLE}.ORDERZONE = 'ES' THEN 'ESP'
        WHEN ${TABLE}.ORDERZONE = 'LK' THEN 'LKA'
        WHEN ${TABLE}.ORDERZONE = 'SD' THEN 'SDN'
        WHEN ${TABLE}.ORDERZONE = 'SR' THEN 'SUR'
        WHEN ${TABLE}.ORDERZONE = 'SJ' THEN 'SJM'
        WHEN ${TABLE}.ORDERZONE = 'SZ' THEN 'SWZ'
        WHEN ${TABLE}.ORDERZONE = 'SE' THEN 'SWE'
        WHEN ${TABLE}.ORDERZONE = 'CH' THEN 'CHE'
        WHEN ${TABLE}.ORDERZONE = 'SY' THEN 'SYR'
        WHEN ${TABLE}.ORDERZONE = 'TW' THEN 'TWN'
        WHEN ${TABLE}.ORDERZONE = 'TJ' THEN 'TJK'
        WHEN ${TABLE}.ORDERZONE = 'TZ' THEN 'TZA'
        WHEN ${TABLE}.ORDERZONE = 'TH' THEN 'THA'
        WHEN ${TABLE}.ORDERZONE = 'TL' THEN 'TLS'
        WHEN ${TABLE}.ORDERZONE = 'TG' THEN 'TGO'
        WHEN ${TABLE}.ORDERZONE = 'TK' THEN 'TKL'
        WHEN ${TABLE}.ORDERZONE = 'TO' THEN 'TON'
        WHEN ${TABLE}.ORDERZONE = 'TT' THEN 'TTO'
        WHEN ${TABLE}.ORDERZONE = 'TN' THEN 'TUN'
        WHEN ${TABLE}.ORDERZONE = 'TR' THEN 'TUR'
        WHEN ${TABLE}.ORDERZONE = 'TM' THEN 'TKM'
        WHEN ${TABLE}.ORDERZONE = 'TC' THEN 'TCA'
        WHEN ${TABLE}.ORDERZONE = 'TV' THEN 'TUV'
        WHEN ${TABLE}.ORDERZONE = 'UG' THEN 'UGA'
        WHEN ${TABLE}.ORDERZONE = 'UA' THEN 'UKR'
        WHEN ${TABLE}.ORDERZONE = 'AE' THEN 'ARE'
        WHEN ${TABLE}.ORDERZONE = 'GB' THEN 'GBR'
        WHEN ${TABLE}.ORDERZONE = 'US' THEN 'USA'
        WHEN ${TABLE}.ORDERZONE = 'UM' THEN 'UMI'
        WHEN ${TABLE}.ORDERZONE = 'UY' THEN 'URY'
        WHEN ${TABLE}.ORDERZONE = 'UZ' THEN 'UZB'
        WHEN ${TABLE}.ORDERZONE = 'VU' THEN 'VUT'
        WHEN ${TABLE}.ORDERZONE = 'VE' THEN 'VEN'
        WHEN ${TABLE}.ORDERZONE = 'VN' THEN 'VNM'
        WHEN ${TABLE}.ORDERZONE = 'VG' THEN 'VGB'
        WHEN ${TABLE}.ORDERZONE = 'VI' THEN 'VIR'
        WHEN ${TABLE}.ORDERZONE = 'WF' THEN 'WLF'
        WHEN ${TABLE}.ORDERZONE = 'EH' THEN 'ESH'
        WHEN ${TABLE}.ORDERZONE = 'YE' THEN 'YEM'
        WHEN ${TABLE}.ORDERZONE = 'ZM' THEN 'ZMB'
        WHEN ${TABLE}.ORDERZONE = 'ZW' THEN 'ZWE'
      ELSE NULL
      END ;;
    }

    dimension: order_zone_map {
      map_layer_name: countries
      sql:
      CASE
        WHEN ${TABLE}.ORDERZONE = 'AF' THEN 'AFG'
        WHEN ${TABLE}.ORDERZONE = 'AX' THEN 'ALA'
        WHEN ${TABLE}.ORDERZONE = 'AL' THEN 'ALB'
        WHEN ${TABLE}.ORDERZONE = 'DZ' THEN 'DZA'
        WHEN ${TABLE}.ORDERZONE = 'AS' THEN 'ASM'
        WHEN ${TABLE}.ORDERZONE = 'AD' THEN 'AND'
        WHEN ${TABLE}.ORDERZONE = 'AO' THEN 'AGO'
        WHEN ${TABLE}.ORDERZONE = 'AI' THEN 'AIA'
        WHEN ${TABLE}.ORDERZONE = 'AQ' THEN 'ATA'
        WHEN ${TABLE}.ORDERZONE = 'AG' THEN 'ATG'
        WHEN ${TABLE}.ORDERZONE = 'AR' THEN 'ARG'
        WHEN ${TABLE}.ORDERZONE = 'AM' THEN 'ARM'
        WHEN ${TABLE}.ORDERZONE = 'AW' THEN 'ABW'
        WHEN ${TABLE}.ORDERZONE = 'AU' THEN 'AUS'
        WHEN ${TABLE}.ORDERZONE = 'AT' THEN 'AUT'
        WHEN ${TABLE}.ORDERZONE = 'AZ' THEN 'AZE'
        WHEN ${TABLE}.ORDERZONE = 'BS' THEN 'BHS'
        WHEN ${TABLE}.ORDERZONE = 'BH' THEN 'BHR'
        WHEN ${TABLE}.ORDERZONE = 'BD' THEN 'BGD'
        WHEN ${TABLE}.ORDERZONE = 'BB' THEN 'BRB'
        WHEN ${TABLE}.ORDERZONE = 'BY' THEN 'BLR'
        WHEN ${TABLE}.ORDERZONE = 'BE' THEN 'BEL'
        WHEN ${TABLE}.ORDERZONE = 'BZ' THEN 'BLZ'
        WHEN ${TABLE}.ORDERZONE = 'BJ' THEN 'BEN'
        WHEN ${TABLE}.ORDERZONE = 'BM' THEN 'BMU'
        WHEN ${TABLE}.ORDERZONE = 'BT' THEN 'BTN'
        WHEN ${TABLE}.ORDERZONE = 'BO' THEN 'BOL'
        WHEN ${TABLE}.ORDERZONE = 'BQ' THEN 'BES'
        WHEN ${TABLE}.ORDERZONE = 'BA' THEN 'BIH'
        WHEN ${TABLE}.ORDERZONE = 'BW' THEN 'BWA'
        WHEN ${TABLE}.ORDERZONE = 'BV' THEN 'BVT'
        WHEN ${TABLE}.ORDERZONE = 'BR' THEN 'BRA'
        WHEN ${TABLE}.ORDERZONE = 'IO' THEN 'IOT'
        WHEN ${TABLE}.ORDERZONE = 'BN' THEN 'BRN'
        WHEN ${TABLE}.ORDERZONE = 'BG' THEN 'BGR'
        WHEN ${TABLE}.ORDERZONE = 'BF' THEN 'BFA'
        WHEN ${TABLE}.ORDERZONE = 'BI' THEN 'BDI'
        WHEN ${TABLE}.ORDERZONE = 'KH' THEN 'KHM'
        WHEN ${TABLE}.ORDERZONE = 'CM' THEN 'CMR'
        WHEN ${TABLE}.ORDERZONE = 'CA' THEN 'CAN'
        WHEN ${TABLE}.ORDERZONE = 'CV' THEN 'CPV'
        WHEN ${TABLE}.ORDERZONE = 'KY' THEN 'CYM'
        WHEN ${TABLE}.ORDERZONE = 'CF' THEN 'CAF'
        WHEN ${TABLE}.ORDERZONE = 'TD' THEN 'TCD'
        WHEN ${TABLE}.ORDERZONE = 'CL' THEN 'CHL'
        WHEN ${TABLE}.ORDERZONE = 'CN' THEN 'CHN'
        WHEN ${TABLE}.ORDERZONE = 'CX' THEN 'CXR'
        WHEN ${TABLE}.ORDERZONE = 'CC' THEN 'CCK'
        WHEN ${TABLE}.ORDERZONE = 'CO' THEN 'COL'
        WHEN ${TABLE}.ORDERZONE = 'KM' THEN 'COM'
        WHEN ${TABLE}.ORDERZONE = 'CG' THEN 'COG'
        WHEN ${TABLE}.ORDERZONE = 'CD' THEN 'COD'
        WHEN ${TABLE}.ORDERZONE = 'CK' THEN 'COK'
        WHEN ${TABLE}.ORDERZONE = 'CR' THEN 'CRI'
        WHEN ${TABLE}.ORDERZONE = 'CI' THEN 'CIV'
        WHEN ${TABLE}.ORDERZONE = 'HR' THEN 'HRV'
        WHEN ${TABLE}.ORDERZONE = 'CU' THEN 'CUB'
        WHEN ${TABLE}.ORDERZONE = 'CW' THEN 'CUW'
        WHEN ${TABLE}.ORDERZONE = 'CY' THEN 'CYP'
        WHEN ${TABLE}.ORDERZONE = 'CZ' THEN 'CZE'
        WHEN ${TABLE}.ORDERZONE = 'DK' THEN 'DNK'
        WHEN ${TABLE}.ORDERZONE = 'DJ' THEN 'DJI'
        WHEN ${TABLE}.ORDERZONE = 'DM' THEN 'DMA'
        WHEN ${TABLE}.ORDERZONE = 'DO' THEN 'DOM'
        WHEN ${TABLE}.ORDERZONE = 'EC' THEN 'ECU'
        WHEN ${TABLE}.ORDERZONE = 'EG' THEN 'EGY'
        WHEN ${TABLE}.ORDERZONE = 'SV' THEN 'SLV'
        WHEN ${TABLE}.ORDERZONE = 'GQ' THEN 'GNQ'
        WHEN ${TABLE}.ORDERZONE = 'ER' THEN 'ERI'
        WHEN ${TABLE}.ORDERZONE = 'EE' THEN 'EST'
        WHEN ${TABLE}.ORDERZONE = 'ET' THEN 'ETH'
        WHEN ${TABLE}.ORDERZONE = 'FK' THEN 'FLK'
        WHEN ${TABLE}.ORDERZONE = 'FO' THEN 'FRO'
        WHEN ${TABLE}.ORDERZONE = 'FJ' THEN 'FJI'
        WHEN ${TABLE}.ORDERZONE = 'FI' THEN 'FIN'
        WHEN ${TABLE}.ORDERZONE = 'FR' THEN 'FRA'
        WHEN ${TABLE}.ORDERZONE = 'GF' THEN 'GUF'
        WHEN ${TABLE}.ORDERZONE = 'PF' THEN 'PYF'
        WHEN ${TABLE}.ORDERZONE = 'TF' THEN 'ATF'
        WHEN ${TABLE}.ORDERZONE = 'GA' THEN 'GAB'
        WHEN ${TABLE}.ORDERZONE = 'GM' THEN 'GMB'
        WHEN ${TABLE}.ORDERZONE = 'GE' THEN 'GEO'
        WHEN ${TABLE}.ORDERZONE = 'DE' THEN 'DEU'
        WHEN ${TABLE}.ORDERZONE = 'GH' THEN 'GHA'
        WHEN ${TABLE}.ORDERZONE = 'GI' THEN 'GIB'
        WHEN ${TABLE}.ORDERZONE = 'GR' THEN 'GRC'
        WHEN ${TABLE}.ORDERZONE = 'GL' THEN 'GRL'
        WHEN ${TABLE}.ORDERZONE = 'GD' THEN 'GRD'
        WHEN ${TABLE}.ORDERZONE = 'GP' THEN 'GLP'
        WHEN ${TABLE}.ORDERZONE = 'GU' THEN 'GUM'
        WHEN ${TABLE}.ORDERZONE = 'GT' THEN 'GTM'
        WHEN ${TABLE}.ORDERZONE = 'GG' THEN 'GGY'
        WHEN ${TABLE}.ORDERZONE = 'GN' THEN 'GIN'
        WHEN ${TABLE}.ORDERZONE = 'GW' THEN 'GNB'
        WHEN ${TABLE}.ORDERZONE = 'GY' THEN 'GUY'
        WHEN ${TABLE}.ORDERZONE = 'HT' THEN 'HTI'
        WHEN ${TABLE}.ORDERZONE = 'HM' THEN 'HMD'
        WHEN ${TABLE}.ORDERZONE = 'VA' THEN 'VAT'
        WHEN ${TABLE}.ORDERZONE = 'HN' THEN 'HND'
        WHEN ${TABLE}.ORDERZONE = 'HK' THEN 'HKG'
        WHEN ${TABLE}.ORDERZONE = 'HU' THEN 'HUN'
        WHEN ${TABLE}.ORDERZONE = 'IS' THEN 'ISL'
        WHEN ${TABLE}.ORDERZONE = 'IN' THEN 'IND'
        WHEN ${TABLE}.ORDERZONE = 'ID' THEN 'IDN'
        WHEN ${TABLE}.ORDERZONE = 'IR' THEN 'IRN'
        WHEN ${TABLE}.ORDERZONE = 'IQ' THEN 'IRQ'
        WHEN ${TABLE}.ORDERZONE = 'IE' THEN 'IRL'
        WHEN ${TABLE}.ORDERZONE = 'IM' THEN 'IMN'
        WHEN ${TABLE}.ORDERZONE = 'IL' THEN 'ISR'
        WHEN ${TABLE}.ORDERZONE = 'IT' THEN 'ITA'
        WHEN ${TABLE}.ORDERZONE = 'JM' THEN 'JAM'
        WHEN ${TABLE}.ORDERZONE = 'JP' THEN 'JPN'
        WHEN ${TABLE}.ORDERZONE = 'JE' THEN 'JEY'
        WHEN ${TABLE}.ORDERZONE = 'JO' THEN 'JOR'
        WHEN ${TABLE}.ORDERZONE = 'KZ' THEN 'KAZ'
        WHEN ${TABLE}.ORDERZONE = 'KE' THEN 'KEN'
        WHEN ${TABLE}.ORDERZONE = 'KI' THEN 'KIR'
        WHEN ${TABLE}.ORDERZONE = 'KP' THEN 'PRK'
        WHEN ${TABLE}.ORDERZONE = 'KR' THEN 'KOR'
        WHEN ${TABLE}.ORDERZONE = 'KW' THEN 'KWT'
        WHEN ${TABLE}.ORDERZONE = 'KG' THEN 'KGZ'
        WHEN ${TABLE}.ORDERZONE = 'LA' THEN 'LAO'
        WHEN ${TABLE}.ORDERZONE = 'LV' THEN 'LVA'
        WHEN ${TABLE}.ORDERZONE = 'LB' THEN 'LBN'
        WHEN ${TABLE}.ORDERZONE = 'LS' THEN 'LSO'
        WHEN ${TABLE}.ORDERZONE = 'LR' THEN 'LBR'
        WHEN ${TABLE}.ORDERZONE = 'LY' THEN 'LBY'
        WHEN ${TABLE}.ORDERZONE = 'LI' THEN 'LIE'
        WHEN ${TABLE}.ORDERZONE = 'LT' THEN 'LTU'
        WHEN ${TABLE}.ORDERZONE = 'LU' THEN 'LUX'
        WHEN ${TABLE}.ORDERZONE = 'MO' THEN 'MAC'
        WHEN ${TABLE}.ORDERZONE = 'MK' THEN 'MKD'
        WHEN ${TABLE}.ORDERZONE = 'MG' THEN 'MDG'
        WHEN ${TABLE}.ORDERZONE = 'MW' THEN 'MWI'
        WHEN ${TABLE}.ORDERZONE = 'MY' THEN 'MYS'
        WHEN ${TABLE}.ORDERZONE = 'MV' THEN 'MDV'
        WHEN ${TABLE}.ORDERZONE = 'ML' THEN 'MLI'
        WHEN ${TABLE}.ORDERZONE = 'MT' THEN 'MLT'
        WHEN ${TABLE}.ORDERZONE = 'MH' THEN 'MHL'
        WHEN ${TABLE}.ORDERZONE = 'MQ' THEN 'MTQ'
        WHEN ${TABLE}.ORDERZONE = 'MR' THEN 'MRT'
        WHEN ${TABLE}.ORDERZONE = 'MU' THEN 'MUS'
        WHEN ${TABLE}.ORDERZONE = 'YT' THEN 'MYT'
        WHEN ${TABLE}.ORDERZONE = 'MX' THEN 'MEX'
        WHEN ${TABLE}.ORDERZONE = 'FM' THEN 'FSM'
        WHEN ${TABLE}.ORDERZONE = 'MD' THEN 'MDA'
        WHEN ${TABLE}.ORDERZONE = 'MC' THEN 'MCO'
        WHEN ${TABLE}.ORDERZONE = 'MN' THEN 'MNG'
        WHEN ${TABLE}.ORDERZONE = 'ME' THEN 'MNE'
        WHEN ${TABLE}.ORDERZONE = 'MS' THEN 'MSR'
        WHEN ${TABLE}.ORDERZONE = 'MA' THEN 'MAR'
        WHEN ${TABLE}.ORDERZONE = 'MZ' THEN 'MOZ'
        WHEN ${TABLE}.ORDERZONE = 'MM' THEN 'MMR'
        WHEN ${TABLE}.ORDERZONE = 'NA' THEN 'NAM'
        WHEN ${TABLE}.ORDERZONE = 'NR' THEN 'NRU'
        WHEN ${TABLE}.ORDERZONE = 'NP' THEN 'NPL'
        WHEN ${TABLE}.ORDERZONE = 'NL' THEN 'NLD'
        WHEN ${TABLE}.ORDERZONE = 'NC' THEN 'NCL'
        WHEN ${TABLE}.ORDERZONE = 'NZ' THEN 'NZL'
        WHEN ${TABLE}.ORDERZONE = 'NI' THEN 'NIC'
        WHEN ${TABLE}.ORDERZONE = 'NE' THEN 'NER'
        WHEN ${TABLE}.ORDERZONE = 'NG' THEN 'NGA'
        WHEN ${TABLE}.ORDERZONE = 'NU' THEN 'NIU'
        WHEN ${TABLE}.ORDERZONE = 'NF' THEN 'NFK'
        WHEN ${TABLE}.ORDERZONE = 'MP' THEN 'MNP'
        WHEN ${TABLE}.ORDERZONE = 'NO' THEN 'NOR'
        WHEN ${TABLE}.ORDERZONE = 'OM' THEN 'OMN'
        WHEN ${TABLE}.ORDERZONE = 'PK' THEN 'PAK'
        WHEN ${TABLE}.ORDERZONE = 'PW' THEN 'PLW'
        WHEN ${TABLE}.ORDERZONE = 'PS' THEN 'PSE'
        WHEN ${TABLE}.ORDERZONE = 'PA' THEN 'PAN'
        WHEN ${TABLE}.ORDERZONE = 'PG' THEN 'PNG'
        WHEN ${TABLE}.ORDERZONE = 'PY' THEN 'PRY'
        WHEN ${TABLE}.ORDERZONE = 'PE' THEN 'PER'
        WHEN ${TABLE}.ORDERZONE = 'PH' THEN 'PHL'
        WHEN ${TABLE}.ORDERZONE = 'PN' THEN 'PCN'
        WHEN ${TABLE}.ORDERZONE = 'PL' THEN 'POL'
        WHEN ${TABLE}.ORDERZONE = 'PT' THEN 'PRT'
        WHEN ${TABLE}.ORDERZONE = 'PR' THEN 'PRI'
        WHEN ${TABLE}.ORDERZONE = 'QA' THEN 'QAT'
        WHEN ${TABLE}.ORDERZONE = 'RE' THEN 'REU'
        WHEN ${TABLE}.ORDERZONE = 'RO' THEN 'ROU'
        WHEN ${TABLE}.ORDERZONE = 'RU' THEN 'RUS'
        WHEN ${TABLE}.ORDERZONE = 'RW' THEN 'RWA'
        WHEN ${TABLE}.ORDERZONE = 'BL' THEN 'BLM'
        WHEN ${TABLE}.ORDERZONE = 'SH' THEN 'SHN'
        WHEN ${TABLE}.ORDERZONE = 'KN' THEN 'KNA'
        WHEN ${TABLE}.ORDERZONE = 'LC' THEN 'LCA'
        WHEN ${TABLE}.ORDERZONE = 'MF' THEN 'MAF'
        WHEN ${TABLE}.ORDERZONE = 'PM' THEN 'SPM'
        WHEN ${TABLE}.ORDERZONE = 'VC' THEN 'VCT'
        WHEN ${TABLE}.ORDERZONE = 'WS' THEN 'WSM'
        WHEN ${TABLE}.ORDERZONE = 'SM' THEN 'SMR'
        WHEN ${TABLE}.ORDERZONE = 'ST' THEN 'STP'
        WHEN ${TABLE}.ORDERZONE = 'SA' THEN 'SAU'
        WHEN ${TABLE}.ORDERZONE = 'SN' THEN 'SEN'
        WHEN ${TABLE}.ORDERZONE = 'RS' THEN 'SRB'
        WHEN ${TABLE}.ORDERZONE = 'SC' THEN 'SYC'
        WHEN ${TABLE}.ORDERZONE = 'SL' THEN 'SLE'
        WHEN ${TABLE}.ORDERZONE = 'SG' THEN 'SGP'
        WHEN ${TABLE}.ORDERZONE = 'SX' THEN 'SXM'
        WHEN ${TABLE}.ORDERZONE = 'SK' THEN 'SVK'
        WHEN ${TABLE}.ORDERZONE = 'SI' THEN 'SVN'
        WHEN ${TABLE}.ORDERZONE = 'SB' THEN 'SLB'
        WHEN ${TABLE}.ORDERZONE = 'SO' THEN 'SOM'
        WHEN ${TABLE}.ORDERZONE = 'ZA' THEN 'ZAF'
        WHEN ${TABLE}.ORDERZONE = 'GS' THEN 'SGS'
        WHEN ${TABLE}.ORDERZONE = 'SS' THEN 'SSD'
        WHEN ${TABLE}.ORDERZONE = 'ES' THEN 'ESP'
        WHEN ${TABLE}.ORDERZONE = 'LK' THEN 'LKA'
        WHEN ${TABLE}.ORDERZONE = 'SD' THEN 'SDN'
        WHEN ${TABLE}.ORDERZONE = 'SR' THEN 'SUR'
        WHEN ${TABLE}.ORDERZONE = 'SJ' THEN 'SJM'
        WHEN ${TABLE}.ORDERZONE = 'SZ' THEN 'SWZ'
        WHEN ${TABLE}.ORDERZONE = 'SE' THEN 'SWE'
        WHEN ${TABLE}.ORDERZONE = 'CH' THEN 'CHE'
        WHEN ${TABLE}.ORDERZONE = 'SY' THEN 'SYR'
        WHEN ${TABLE}.ORDERZONE = 'TW' THEN 'TWN'
        WHEN ${TABLE}.ORDERZONE = 'TJ' THEN 'TJK'
        WHEN ${TABLE}.ORDERZONE = 'TZ' THEN 'TZA'
        WHEN ${TABLE}.ORDERZONE = 'TH' THEN 'THA'
        WHEN ${TABLE}.ORDERZONE = 'TL' THEN 'TLS'
        WHEN ${TABLE}.ORDERZONE = 'TG' THEN 'TGO'
        WHEN ${TABLE}.ORDERZONE = 'TK' THEN 'TKL'
        WHEN ${TABLE}.ORDERZONE = 'TO' THEN 'TON'
        WHEN ${TABLE}.ORDERZONE = 'TT' THEN 'TTO'
        WHEN ${TABLE}.ORDERZONE = 'TN' THEN 'TUN'
        WHEN ${TABLE}.ORDERZONE = 'TR' THEN 'TUR'
        WHEN ${TABLE}.ORDERZONE = 'TM' THEN 'TKM'
        WHEN ${TABLE}.ORDERZONE = 'TC' THEN 'TCA'
        WHEN ${TABLE}.ORDERZONE = 'TV' THEN 'TUV'
        WHEN ${TABLE}.ORDERZONE = 'UG' THEN 'UGA'
        WHEN ${TABLE}.ORDERZONE = 'UA' THEN 'UKR'
        WHEN ${TABLE}.ORDERZONE = 'AE' THEN 'ARE'
        WHEN ${TABLE}.ORDERZONE = 'GB' THEN 'GBR'
        WHEN ${TABLE}.ORDERZONE = 'US' THEN 'USA'
        WHEN ${TABLE}.ORDERZONE = 'UM' THEN 'UMI'
        WHEN ${TABLE}.ORDERZONE = 'UY' THEN 'URY'
        WHEN ${TABLE}.ORDERZONE = 'UZ' THEN 'UZB'
        WHEN ${TABLE}.ORDERZONE = 'VU' THEN 'VUT'
        WHEN ${TABLE}.ORDERZONE = 'VE' THEN 'VEN'
        WHEN ${TABLE}.ORDERZONE = 'VN' THEN 'VNM'
        WHEN ${TABLE}.ORDERZONE = 'VG' THEN 'VGB'
        WHEN ${TABLE}.ORDERZONE = 'VI' THEN 'VIR'
        WHEN ${TABLE}.ORDERZONE = 'WF' THEN 'WLF'
        WHEN ${TABLE}.ORDERZONE = 'EH' THEN 'ESH'
        WHEN ${TABLE}.ORDERZONE = 'YE' THEN 'YEM'
        WHEN ${TABLE}.ORDERZONE = 'ZM' THEN 'ZMB'
        WHEN ${TABLE}.ORDERZONE = 'ZW' THEN 'ZWE'
      ELSE NULL
      END ;;
    }

    dimension_group: payment {
      type: time
      timeframes: [
        raw,
        date,
        week,
        month,
        quarter,
        year
      ]
      convert_tz: no
      datatype: date
      sql: ${TABLE}.PAYMENTDATE ;;
    }

    dimension: payment_provider {
      type: string
      sql: ${TABLE}.PAYMENTPROVIDER ;;
    }

    dimension: payment_ref {
      type: string
      sql: ${TABLE}.PAYMENTREF ;;
    }

    dimension: total {
      type: number
      sql: ${TABLE}.TOTAL ;;
    }

    dimension: transaction_country {
      type: string
      sql: ${TABLE}.TRANSACTIONCOUNTRY ;;
    }

    dimension: type {
      type: string
      sql: ${TABLE}.TYPE ;;
    }

    dimension: user_id {
      hidden: no
      type: string
      sql: ${TABLE}.USERID ;;
    }

    dimension: user_login {
      type: string
      sql: ${TABLE}.USERLOGIN ;;
    }

    dimension: user_zone {
      type: string
      sql: ${TABLE}.USERZONE ;;
    }

    # added to have a uinique field and make it possible to self-join the view
#     dimension: row_number {
#       type: number
#       primary_key: yes
#       sql: ${TABLE}.ROW_NUMBER ;;
#     }

    measure: count {
      type: count
      drill_fields: []
    }

    measure: total_sum {
      type: sum
      sql: ${total} ;;
    }

# view: total_sum_for_previous_period {
#   derived_table: {
# #   measure: total_sum_for_previous_period {
# #     type: number
#     sql:
#     (
#     SELECT SUM(TOTAL) FROM PUBLIC.SALES_GLOBAL
#     WHERE
#     CASE
#     WHEN {% parameter date_granularity %} = 'Past Week' THEN
#     (sales_global.ORDERDATE  >= TO_DATE(DATEADD('day', -13, CURRENT_DATE() )) ) AND ( sales_global.ORDERDATE < TO_DATE(DATEADD('day', 7, DATEADD('day', -13, CURRENT_DATE() ))) )
#     WHEN {% parameter date_granularity %} = 'Past Month' THEN
#     (sales_global.ORDERDATE  >= TO_DATE(DATEADD('day', -59, CURRENT_DATE() )) ) AND ( sales_global.ORDERDATE < TO_DATE(DATEADD('day', 30, DATEADD('day', -59, CURRENT_DATE() ))) )
#     WHEN {% parameter date_granularity %} = 'Past Quarter' THEN
#     (sales_global.ORDERDATE  >= TO_DATE(DATEADD('day', -119, CURRENT_DATE() )) ) AND ( sales_global.ORDERDATE < TO_DATE(DATEADD('day', 60, DATEADD('day', -119, CURRENT_DATE() ))) )
#     WHEN {% parameter date_granularity %} = 'Past Year' THEN
#     (sales_global.ORDERDATE  >= TO_DATE(DATEADD('day', -729, CURRENT_DATE() )) ) AND ( sales_global.ORDERDATE < TO_DATE(DATEADD('day', 365, DATEADD('day', -729, CURRENT_DATE() ))) )
#     ELSE NULL
#     END
#     );;
#   }

#       sql:
#     (
#     CASE
#     WHEN {% parameter date_granularity %} = 'Past Week' THEN
#     SELECT SUM(TOTAL) FROM PUBLIC.SALES_GLOBAL WHERE
#     (sales_global.ORDERDATE  >= TO_DATE(DATEADD('day', -13, CURRENT_DATE() )) ) AND ( sales_global.ORDERDATE < TO_DATE(DATEADD('day', 7, DATEADD('day', -13, CURRENT_DATE() ))) )
#     WHEN {% parameter date_granularity %} = 'Past Month' THEN
#     SELECT SUM(TOTAL) FROM PUBLIC.SALES_GLOBAL WHERE
#     (sales_global.ORDERDATE  >= TO_DATE(DATEADD('day', -59, CURRENT_DATE() )) ) AND ( sales_global.ORDERDATE < TO_DATE(DATEADD('day', 30, DATEADD('day', -59, CURRENT_DATE() ))) )
#     WHEN {% parameter date_granularity %} = 'Past Quarter' THEN
#       SELECT SUM(TOTAL) FROM PUBLIC.SALES_GLOBAL WHERE
#     (sales_global.ORDERDATE  >= TO_DATE(DATEADD('day', -119, CURRENT_DATE() )) ) AND ( sales_global.ORDERDATE < TO_DATE(DATEADD('day', 60, DATEADD('day', -119, CURRENT_DATE() ))) )
#   WHEN {% parameter date_granularity %} = 'Past Year' THEN
#       SELECT SUM(TOTAL) FROM PUBLIC.SALES_GLOBAL WHERE
#     (sales_global.ORDERDATE  >= TO_DATE(DATEADD('day', -729, CURRENT_DATE() )) ) AND ( sales_global.ORDERDATE < TO_DATE(DATEADD('day', 365, DATEADD('day', -729, CURRENT_DATE() ))) )
#     ELSE NULL
#     END
#     );;
#   }

    # added to have a parametrized filter on UI
    parameter: date_granularity {
      type: string
      allowed_value: { value: "Past Week" }
      allowed_value: { value: "Past Month" }
      allowed_value: { value: "Past Quarter" }
      allowed_value: { value: "Past Year" }
    }

  }
