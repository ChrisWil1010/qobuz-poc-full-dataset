- dashboard: sales_global_dashboard
  title: Sales Global dashboard
  layout: newspaper
  elements:
  - title: Sales by Payment Provider for the selected period
    name: Sales by Payment Provider for the selected period
    model: poc_model
    explore: sales_global
    type: looker_pie
    fields:
    - sales_global.payment_provider
    - sales_global.total_sum
    filters:
      sales_global.date_granularity: Past Week
    sorts:
    - sales_global.total_sum desc
    limit: 500
    query_timezone: Europe/London
    value_labels: legend
    label_type: labPer
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: true
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    inner_radius: 45
    hidden_fields: []
    y_axes: []
    note_state: collapsed
    note_display: above
    note_text: ''
    row: 6
    col: 0
    width: 8
    height: 6
  - title: sales_global_total_sum_single_number
    name: sales_global_total_sum_single_number
    model: poc_model
    explore: sales_global
    type: single_value
    fields:
    - sales_global.total_sum
    filters: {}
    sorts:
    - goal_value
    limit: 500
    column_limit: 50
    dynamic_fields:
    - table_calculation: goal_value
      label: goal value
      expression: '300000'
      value_format:
      value_format_name: eur_0
      _kind_hint: dimension
      _type_hint: number
    query_timezone: Europe/London
    custom_color_enabled: false
    custom_color: forestgreen
    show_single_value_title: false
    show_comparison: true
    comparison_type: progress_percentage
    comparison_reverse_colors: false
    show_comparison_label: true
    map_plot_mode: points
    heatmap_gridlines: false
    heatmap_gridlines_empty: false
    heatmap_opacity: 0.5
    show_region_field: true
    draw_map_labels_above_data: true
    map_tile_provider: positron
    map_position: fit_data
    map_scale_indicator: 'off'
    map_pannable: true
    map_zoomable: true
    map_marker_type: circle
    map_marker_icon_name: default
    map_marker_radius_mode: proportional_value
    map_marker_units: meters
    map_marker_proportional_scale_type: linear
    map_marker_color_mode: fixed
    show_view_names: true
    show_legend: true
    quantize_map_value_colors: false
    reverse_map_value_colors: false
    value_labels: legend
    label_type: labPer
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: false
    y_axis_gridlines: true
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    show_null_points: true
    point_style: circle
    interpolation: linear
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    ordering: none
    show_null_labels: false
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    single_value_title: ''
    value_format: "#,##0 €"
    series_types: {}
    hidden_series: []
    comparison_label: goal value
    listen:
      Period: sales_global.date_granularity
      Country: sales_global.order_zone_map
    note_state: collapsed
    note_display: below
    note_text: Sales Total Sum for the selected period (€)
    row: 0
    col: 0
    width: 8
    height: 6
  - title: sales_global_total_sum_per_type_plot
    name: sales_global_total_sum_per_type_plot
    model: poc_model
    explore: sales_global
    type: looker_line
    fields:
    - sales_global.total_sum
    - sales_global.order_date
    - sales_global.type
    pivots:
    - sales_global.type
    fill_fields:
    - sales_global.order_date
    filters: {}
    sorts:
    - sales_global.order_date desc
    - sales_global.type
    limit: 500
    total: true
    row_total: right
    query_timezone: Europe/London
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: false
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    show_null_points: true
    point_style: none
    interpolation: linear
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    hide_legend: false
    colors:
    - "#62bad4"
    - "#a2e81e"
    - "#929292"
    - "#9fdee0"
    - "#1f3e5a"
    - "#90c8ae"
    - "#92818d"
    - "#c5c6a6"
    - "#82c2ca"
    - "#cee0a0"
    - "#928fb4"
    - "#9fc190"
    series_colors:
      sales_global.total_sum: "#45b32b"
      Subscription - sales_global.total_sum: "#75c8e0"
      Download - sales_global.total_sum: "#8ac227"
      SpecialOffer - sales_global.total_sum: "#ff9900"
    y_axes:
    - label: Total Sales (€)
      orientation: left
      showLabels: true
      showValues: true
      tickDensity: default
      type: linear
      unpinAxis: false
      series:
      - id: Download
        name: Download
        axisId: sales_global.total_sum
      - id: SpecialOffer
        name: SpecialOffer
        axisId: sales_global.total_sum
      - id: Subscription
        name: Subscription
        axisId: sales_global.total_sum
      - id: Row Total
        name: Row Total
        axisId: sales_global.total_sum
    hidden_series:
    - Row Total
    hidden_fields: []
    listen:
      Period: sales_global.date_granularity
      Country: sales_global.order_zone_map
    note_state: collapsed
    note_display: above
    note_text: Sales Total Sum by Types for the selected period
    title_hidden: true
    row: 0
    col: 16
    width: 8
    height: 6
  - title: sales_global_total_sum_types
    name: sales_global_total_sum_types
    model: poc_model
    explore: sales_global
    type: looker_pie
    fields:
    - sales_global.total_sum
    - sales_global.type
    sorts:
    - sales_global.total_sum desc
    limit: 500
    total: true
    row_total: right
    query_timezone: Europe/London
    value_labels: labels
    label_type: labPer
    map_plot_mode: points
    heatmap_gridlines: false
    heatmap_gridlines_empty: false
    heatmap_opacity: 0.5
    show_region_field: true
    draw_map_labels_above_data: true
    map_tile_provider: positron
    map_position: fit_data
    map_scale_indicator: 'off'
    map_pannable: true
    map_zoomable: true
    map_marker_type: circle
    map_marker_icon_name: default
    map_marker_radius_mode: proportional_value
    map_marker_units: meters
    map_marker_proportional_scale_type: linear
    map_marker_color_mode: fixed
    show_view_names: true
    show_legend: true
    quantize_map_value_colors: false
    reverse_map_value_colors: false
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    limit_displayed_rows: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: false
    y_axis_gridlines: true
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    show_null_points: true
    point_style: circle
    custom_color_enabled: false
    custom_color: forestgreen
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    series_types: {}
    series_colors:
      SpecialOffer: "#ff9900"
      Subscription: "#75c8e0"
      Download: "#8ac227"
    listen:
      Period: sales_global.date_granularity
      Country: sales_global.order_zone_map
    note_state: collapsed
    note_display: above
    note_text: Sales Total Sum by Types for the selected period
    title_hidden: true
    row: 0
    col: 8
    width: 8
    height: 6
  - title: sales_global_total_sum_per_type_table
    name: sales_global_total_sum_per_type_table
    model: poc_model
    explore: sales_global
    type: table
    fields:
    - sales_global.total_sum
    - sales_global.order_date
    - sales_global.type
    pivots:
    - sales_global.type
    fill_fields:
    - sales_global.order_date
    filters: {}
    sorts:
    - sales_global.order_date desc
    - sales_global.type
    limit: 500
    total: true
    row_total: right
    query_timezone: Europe/London
    show_view_names: false
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    limit_displayed_rows: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    show_null_points: true
    point_style: none
    interpolation: linear
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    hide_legend: false
    colors:
    - "#62bad4"
    - "#a2e81e"
    - "#929292"
    - "#9fdee0"
    - "#1f3e5a"
    - "#90c8ae"
    - "#92818d"
    - "#c5c6a6"
    - "#82c2ca"
    - "#cee0a0"
    - "#928fb4"
    - "#9fc190"
    series_colors:
      sales_global.total_sum: "#45b32b"
      Subscription - sales_global.total_sum: "#75c8e0"
      Download - sales_global.total_sum: "#8ac227"
      SpecialOffer - sales_global.total_sum: "#ff9900"
    y_axes:
    - label: ''
      maxValue:
      minValue:
      orientation: left
      showLabels: true
      showValues: true
      tickDensity: default
      tickDensityCustom: 5
      type: linear
      unpinAxis: false
      valueFormat: ''
      series:
      - id: sales_global.total_sum
        name: Stage Sales Global Total Sum
        axisId: sales_global.total_sum
    hidden_series:
    - Row Total
    hidden_fields: []
    listen:
      Period: sales_global.date_granularity
      Country: sales_global.order_zone_map
    note_state: collapsed
    note_display: above
    note_text: Sales Total Sum by Types for the selected period (€)
    title_hidden: true
    row: 6
    col: 16
    width: 8
    height: 6
  - title: sales_global_total_sum_N-1_plot
    name: sales_global_total_sum_N-1_plot
    model: poc_model
    explore: sales_global
    type: looker_area
    fields:
    - sales_global.order_date
    - sales_global.total_sum
    filters: {}
    sorts:
    - sales_global.order_date desc
    limit: 500
    column_limit: 50
    query_timezone: Europe/London
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: false
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    show_null_points: true
    point_style: none
    interpolation: step
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    ordering: none
    show_null_labels: false
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    hidden_series:
    - n_1
    focus_on_hover: false
    hide_legend: false
    colors:
    - 'palette: Looker Classic'
    series_colors:
      sales_global.total_sum: "#000000"
      sales_global_auxiliary.total_sum_for_previous_period: "#8f8f8f"
    y_axes:
    - label: Total Sales (€)
      maxValue:
      minValue:
      orientation: left
      showLabels: true
      showValues: true
      tickDensity: default
      tickDensityCustom: 5
      type: linear
      unpinAxis: false
      valueFormat:
      series:
      - id: sales_global.total_sum
        name: Total Sum
        axisId: sales_global.total_sum
    x_axis_label: Order Date
    hidden_fields: []
    listen:
      Period: sales_global.date_granularity
      Country: sales_global.order_zone_map
    note_state: collapsed
    note_display: above
    note_text: Sales Total Sum for the selected period
    title_hidden: true
    row: 6
    col: 8
    width: 8
    height: 6
  - title: sales_global_total_sum_eu_map
    name: sales_global_total_sum_eu_map
    model: poc_model
    explore: sales_global
    type: looker_map
    fields:
    - sales_global.total_sum
    - sales_global.order_zone_map
    filters:
      sales_global.order_date: ''
    sorts:
    - sales_global.total_sum desc
    limit: 500
    query_timezone: Europe/London
    map_plot_mode: points
    heatmap_gridlines: false
    heatmap_gridlines_empty: false
    heatmap_opacity: 0.9
    show_region_field: true
    draw_map_labels_above_data: true
    map_tile_provider: positron
    map_position: fit_data
    map_scale_indicator: 'off'
    map_pannable: true
    map_zoomable: true
    map_marker_type: circle
    map_marker_icon_name: default
    map_marker_radius_mode: proportional_value
    map_marker_units: meters
    map_marker_proportional_scale_type: linear
    map_marker_color_mode: fixed
    show_view_names: true
    show_legend: true
    quantize_map_value_colors: false
    reverse_map_value_colors: false
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: false
    y_axis_gridlines: true
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    map_latitude: 37.78808138412046
    map_longitude: -15.644531250000002
    map_zoom: 3
    map_value_colors:
    - lightgreen
    - darkgreen
    hidden_fields: []
    y_axes: []
    listen:
      Period: sales_global.date_granularity
      Country: sales_global.order_zone_map
    note_state: collapsed
    note_display: above
    note_text: Sales Total Sum per countries map
    title_hidden: true
    row: 12
    col: 0
    width: 24
    height: 13
  - title: sales_global_total_sum_eu_table
    name: sales_global_total_sum_eu_table
    model: poc_model
    explore: sales_global
    type: table
    fields:
    - sales_global.total_sum
    - sales_global.order_zone_map
    - sales_global.type
    pivots:
    - sales_global.type
    filters:
      sales_global.order_date: ''
    sorts:
    - sales_global.total_sum desc 0
    - sales_global.type 0
    limit: 500
    total: true
    row_total: right
    query_timezone: Europe/London
    show_view_names: false
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    limit_displayed_rows: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    map_plot_mode: points
    heatmap_gridlines: false
    heatmap_gridlines_empty: false
    heatmap_opacity: 0.5
    show_region_field: true
    draw_map_labels_above_data: true
    map_tile_provider: positron
    map_position: custom
    map_scale_indicator: 'off'
    map_pannable: true
    map_zoomable: true
    map_marker_type: circle
    map_marker_icon_name: default
    map_marker_radius_mode: proportional_value
    map_marker_units: meters
    map_marker_proportional_scale_type: linear
    map_marker_color_mode: fixed
    show_legend: true
    quantize_map_value_colors: false
    reverse_map_value_colors: false
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: false
    y_axis_gridlines: true
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    map_latitude: 47.040182144806664
    map_longitude: 24.433593750000004
    map_zoom: 3
    hidden_fields: []
    y_axes: []
    listen:
      Period: sales_global.date_granularity
      Country: sales_global.order_zone_map
    note_state: collapsed
    note_display: above
    note_text: Sales Total Sum per countries
    title_hidden: true
    row: 25
    col: 0
    width: 24
    height: 8
  filters:
  - name: Period
    title: Period
    type: field_filter
    default_value: Past Week
    allow_multiple_values: true
    required: false
    model: poc_model
    explore: sales_global
    listens_to_filters: []
    field: sales_global.date_granularity
  - name: Country
    title: Country
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: poc_model
    explore: sales_global
    listens_to_filters: []
    field: sales_global.order_zone_map
