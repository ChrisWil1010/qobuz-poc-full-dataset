view: subscriptions {
  sql_table_name: PUBLIC.SUBSCRIPTIONS ;;

  dimension_group: cancel {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.CANCELDATE ;;
  }

  dimension: cancel_reason {
    type: string
    sql: ${TABLE}.CANCELREASON ;;
  }

  dimension_group: convert {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.CONVERTDATE ;;
  }

  dimension: convert_to_abo_id {
    hidden: no
    type: number
    value_format_name: id
    sql: ${TABLE}.CONVERTTOABOID ;;
  }

  dimension: convert_to_offer {
    type: number
    sql: ${TABLE}.CONVERTTOOFFER ;;
  }

  dimension: convert_to_periodicity {
    type: number
    sql: ${TABLE}.CONVERTTOPERIODICITY ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.CURRENCY ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.ENDDATE ;;
  }

  dimension: file_version {
    type: string
    sql: ${TABLE}.FILEVERSION ;;
  }

  dimension: offer {
    type: string
    sql: ${TABLE}.OFFER ;;
  }

  dimension: partner {
    type: string
    sql: ${TABLE}.PARTNER ;;
  }

  dimension: periodicity {
    type: string
    sql: ${TABLE}.PERIODICITY ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.STARTDATE ;;
  }

  dimension_group: start_date_with_trial_period {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.STARTDATEWITHTRIALPERIOD ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.STATUS ;;
  }

#   dimension: sub_id { hidden: yes
#     type: string
#     sql: ${TABLE}.SUBID ;;
#   }

  dimension: trial_length_in_days {
    type: number
    sql: ${TABLE}.TRIALLENGHTINDAYS ;;
  }

  dimension_group: trial_period_end_date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.TRIALPERIODENDDATE ;;
  }

  dimension: try_and_buy_in_progress {
    type: number
    sql: ${TABLE}.TRYANDBUYINPROGRESS ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.TYPE ;;
  }

  dimension: user_id {
    hidden: no
    type: string
    sql: ${TABLE}.USERID ;;
  }

  dimension: yearly_value {
    type: number
    sql: ${TABLE}.YEARLYVALUE ;;
  }

  dimension: zone {
    type: string
    sql: ${TABLE}.ZONE ;;
  }

  dimension: sub_id {
    type: string
    sql: ${TABLE}.SUBID ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }

  measure: distinct_subscriptions {
    type: count_distinct
    sql: ${sub_id} ;;
  }

  measure: distinct_subscribers {
    type: count_distinct
    sql: ${user_id} ;;
  }

  measure: yearly_value_sum {
    type: sum
    sql: ${yearly_value} ;;
  }
}
