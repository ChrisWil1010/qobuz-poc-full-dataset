- dashboard: streaming_dashboard
  title: Streaming dashboard
  layout: newspaper
  elements:
  - name: Number of Streams
    title: Number of Streams
    model: 
    explore: streaming
    type: single_value
    fields:
    - streaming.stream_count
    limit: 500
    query_timezone: Europe/London
    custom_color_enabled: false
    custom_color: forestgreen
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    value_format: "#,##0"
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    row: 0
    col: 6
    width: 6
    height: 3
  - name: Number of Streaming Customers
    title: Number of Streaming Customers
    model: 
    explore: streaming
    type: single_value
    fields:
    - streaming.distinct_customers
    limit: 500
    query_timezone: Europe/London
    custom_color_enabled: false
    custom_color: forestgreen
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    row: 0
    col: 0
    width: 6
    height: 3
  - name: Number of Hours Streamed
    title: Number of Hours Streamed
    model: 
    explore: streaming
    type: single_value
    fields:
    - streaming.stream_duration_hours_sum
    limit: 500
    query_timezone: Europe/London
    custom_color_enabled: false
    custom_color: forestgreen
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    value_format: '#,##0 "hours"'
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    row: 0
    col: 12
    width: 6
    height: 3
  - name: Total Amount of Streaming Royalties
    title: Total Amount of Streaming Royalties
    model: 
    explore: streaming
    type: single_value
    fields:
    - streaming.royalty_amount_sum
    limit: 500
    query_timezone: Europe/London
    custom_color_enabled: false
    custom_color: forestgreen
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    value_format: "#,##0 €"
    hidden_fields: []
    y_axes: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    row: 0
    col: 18
    width: 6
    height: 3
  - name: Stream Duration by Application and Country (top apps & countries)
    title: Stream Duration by Application and Country (top apps & countries)
    model: 
    explore: streaming
    type: looker_column
    fields:
    - streaming.app_name
    - streaming.user_zone
    - streaming.stream_duration_hours_sum
    pivots:
    - streaming.app_name
    filters:
      streaming.app_name: Qobuz Mobile v3 - iOS - Prod,Qobuz Desktop v4 - Prod,Qobuz
        Player v4 - Prod,Sonos,Qobuz Mobile v3 - Android - Prod
    sorts:
    - streaming.app_name
    - streaming.stream_duration_hours_sum desc 0
    limit: 10
    column_limit: 50
    query_timezone: Europe/London
    stacking: normal
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: true
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    show_null_points: true
    point_style: circle_outline
    interpolation: linear
    map_plot_mode: points
    heatmap_gridlines: false
    heatmap_gridlines_empty: false
    heatmap_opacity: 0.5
    show_region_field: true
    draw_map_labels_above_data: true
    map_tile_provider: light
    map_position: custom
    map_scale_indicator: 'off'
    map_pannable: true
    map_zoomable: true
    map_marker_type: circle
    map_marker_icon_name: default
    map_marker_radius_mode: proportional_value
    map_marker_units: meters
    map_marker_proportional_scale_type: linear
    map_marker_color_mode: fixed
    show_legend: true
    quantize_map_value_colors: false
    reverse_map_value_colors: false
    value_labels: labels
    label_type: labPer
    custom_color_enabled: false
    custom_color: forestgreen
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    leftAxisLabelVisible: false
    leftAxisLabel: ''
    rightAxisLabelVisible: false
    rightAxisLabel: ''
    barColors:
    - red
    - blue
    smoothedBars: false
    orientation: automatic
    labelPosition: left
    percentType: total
    percentPosition: inline
    valuePosition: right
    labelColorEnabled: false
    labelColor: "#FFF"
    font_size: '12'
    series_types: {}
    hidden_series:
    - streaming.stream_duration_sum
    hide_legend: false
    series_colors: {}
    y_axes:
    - label: Stream Duration (hrs)
      orientation: left
      series:
      - id: Qobuz Desktop v4 - Prod
        name: Qobuz Desktop v4 - Prod
        axisId: streaming.stream_duration_hours_sum
      - id: Qobuz Mobile v3 - Android - Prod
        name: Qobuz Mobile v3 - Android - Prod
        axisId: streaming.stream_duration_hours_sum
      - id: Qobuz Mobile v3 - iOS - Prod
        name: Qobuz Mobile v3 - iOS - Prod
        axisId: streaming.stream_duration_hours_sum
      - id: Qobuz Player v4 - Prod
        name: Qobuz Player v4 - Prod
        axisId: streaming.stream_duration_hours_sum
      - id: Sonos
        name: Sonos
        axisId: streaming.stream_duration_hours_sum
      showLabels: true
      showValues: true
      unpinAxis: false
      tickDensity: default
      tickDensityCustom: 5
      type: linear
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    colors:
    - 'palette: Looker Classic'
    conditional_formatting:
    - type: low to high
      value:
      background_color:
      font_color:
      palette:
        name: Red to Yellow to Green
        colors:
        - "#F36254"
        - "#FCF758"
        - "#4FBC89"
      bold: false
      italic: false
      strikethrough: false
      fields:
    value_format: "#,##0  €"
    map_latitude: 51.998410382390325
    map_longitude: 10.502929687500002
    map_zoom: 4
    label_value_format: ''
    hidden_fields: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    row: 3
    col: 10
    width: 14
    height: 8
  - name: Top 10 Apps by Stream Duration
    title: Top 10 Apps by Stream Duration
    model: 
    explore: streaming
    type: looker_pie
    fields:
    - streaming.app_name
    - streaming.stream_duration_hours_sum
    filters: {}
    sorts:
    - streaming.stream_duration_hours_sum desc
    limit: 10
    column_limit: 50
    query_timezone: Europe/London
    value_labels: labels
    label_type: labPer
    stacking: normal
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: true
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    show_null_points: true
    point_style: circle_outline
    interpolation: linear
    map_plot_mode: points
    heatmap_gridlines: false
    heatmap_gridlines_empty: false
    heatmap_opacity: 0.5
    show_region_field: true
    draw_map_labels_above_data: true
    map_tile_provider: light
    map_position: custom
    map_scale_indicator: 'off'
    map_pannable: true
    map_zoomable: true
    map_marker_type: circle
    map_marker_icon_name: default
    map_marker_radius_mode: proportional_value
    map_marker_units: meters
    map_marker_proportional_scale_type: linear
    map_marker_color_mode: fixed
    show_legend: true
    quantize_map_value_colors: false
    reverse_map_value_colors: false
    custom_color_enabled: false
    custom_color: forestgreen
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    leftAxisLabelVisible: false
    leftAxisLabel: ''
    rightAxisLabelVisible: false
    rightAxisLabel: ''
    barColors:
    - red
    - blue
    smoothedBars: false
    orientation: automatic
    labelPosition: left
    percentType: total
    percentPosition: inline
    valuePosition: right
    labelColorEnabled: false
    labelColor: "#FFF"
    font_size: '12'
    series_types: {}
    hidden_series:
    - streaming.stream_duration_sum
    hide_legend: false
    series_colors: {}
    y_axes:
    - label: Stream Duration (hrs)
      orientation: left
      series:
      - id: Qobuz Desktop v4 - Prod
        name: Qobuz Desktop v4 - Prod
        axisId: streaming.stream_duration_hours_sum
      - id: Qobuz Mobile v3 - Android - Prod
        name: Qobuz Mobile v3 - Android - Prod
        axisId: streaming.stream_duration_hours_sum
      - id: Qobuz Mobile v3 - iOS - Prod
        name: Qobuz Mobile v3 - iOS - Prod
        axisId: streaming.stream_duration_hours_sum
      - id: Qobuz Player v4 - Prod
        name: Qobuz Player v4 - Prod
        axisId: streaming.stream_duration_hours_sum
      - id: Sonos
        name: Sonos
        axisId: streaming.stream_duration_hours_sum
      showLabels: true
      showValues: true
      unpinAxis: false
      tickDensity: default
      tickDensityCustom: 5
      type: linear
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    colors:
    - 'palette: Looker Classic'
    conditional_formatting:
    - type: low to high
      value:
      background_color:
      font_color:
      palette:
        name: Red to Yellow to Green
        colors:
        - "#F36254"
        - "#FCF758"
        - "#4FBC89"
      bold: false
      italic: false
      strikethrough: false
      fields:
    value_format: "#,##0  €"
    map_latitude: 51.998410382390325
    map_longitude: 10.502929687500002
    map_zoom: 4
    label_value_format: ''
    inner_radius: 55
    hidden_fields: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    row: 3
    col: 0
    width: 10
    height: 8
  - name: Top 10 Suppliers by  Royalties Due
    title: Top 10 Suppliers by  Royalties Due
    model: 
    explore: streaming
    type: looker_pie
    fields:
    - streaming.supplier
    - streaming.royalty_amount_sum
    filters: {}
    sorts:
    - streaming.royalty_amount_sum desc
    limit: 10
    column_limit: 50
    query_timezone: Europe/London
    value_labels: labels
    label_type: labPer
    stacking: normal
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: true
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    show_null_points: true
    point_style: circle_outline
    interpolation: linear
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    custom_color_enabled: false
    custom_color: forestgreen
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    ordering: none
    show_null_labels: false
    leftAxisLabelVisible: false
    leftAxisLabel: ''
    rightAxisLabelVisible: false
    rightAxisLabel: ''
    barColors:
    - red
    - blue
    smoothedBars: false
    orientation: automatic
    labelPosition: left
    percentType: total
    percentPosition: inline
    valuePosition: right
    labelColorEnabled: false
    labelColor: "#FFF"
    font_size: '12'
    series_types: {}
    hidden_series:
    - streaming.stream_duration_sum
    hide_legend: false
    series_colors: {}
    y_axes:
    - label: Royalty Amount Sum
      orientation: left
      series:
      - id: streaming.royalty_amount_sum
        name: Stage Streaming Royalty Amount Sum
        axisId: streaming.royalty_amount_sum
      - id: streaming.stream_count
        name: Stage Streaming Stream Count
        axisId: streaming.stream_count
      - id: streaming.stream_duration_sum
        name: Stage Streaming Stream Duration Sum
        axisId: streaming.stream_duration_sum
      - id: streaming.distinct_customers
        name: Stage Streaming Distinct Customers
        axisId: streaming.distinct_customers
      showLabels: true
      showValues: true
      unpinAxis: false
      tickDensity: default
      tickDensityCustom: 5
      type: linear
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    colors:
    - 'palette: Looker Classic'
    conditional_formatting:
    - type: low to high
      value:
      background_color:
      font_color:
      palette:
        name: Red to Yellow to Green
        colors:
        - "#F36254"
        - "#FCF758"
        - "#4FBC89"
      bold: false
      italic: false
      strikethrough: false
      fields:
    value_format: "#,##0  €"
    hidden_fields: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    row: 11
    col: 0
    width: 8
    height: 7
  - name: Top 10 Album Artists by Number of Streaming Customers
    title: Top 10 Album Artists by Number of Streaming Customers
    model: 
    explore: streaming
    type: looker_pie
    fields:
    - streaming.album_artist_name
    - streaming.distinct_customers
    filters: {}
    sorts:
    - streaming.distinct_customers desc
    limit: 10
    column_limit: 50
    query_timezone: Europe/London
    value_labels: labels
    label_type: labPer
    stacking: normal
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: true
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    show_null_points: true
    point_style: circle_outline
    interpolation: linear
    leftAxisLabelVisible: false
    leftAxisLabel: ''
    rightAxisLabelVisible: false
    rightAxisLabel: ''
    barColors:
    - red
    - blue
    smoothedBars: false
    orientation: automatic
    labelPosition: left
    percentType: total
    percentPosition: inline
    valuePosition: right
    labelColorEnabled: false
    labelColor: "#FFF"
    font_size: '12'
    custom_color_enabled: false
    custom_color: forestgreen
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    hidden_series:
    - streaming.stream_duration_sum
    hide_legend: false
    series_colors: {}
    y_axes:
    - label: Royalty Amount Sum
      orientation: left
      series:
      - id: streaming.royaltyamount_sum
        name: Stage Streaming Royaltyamount Sum
        axisId: streaming.royaltyamount_sum
      - id: streaming.stream_count
        name: Stage Streaming Stream Count
        axisId: streaming.stream_count
      - id: streaming.stream_duration_sum
        name: Stage Streaming Stream Duration Sum
        axisId: streaming.stream_duration_sum
      - id: streaming.distinct_customers
        name: Stage Streaming Distinct Customers
        axisId: streaming.distinct_customers
      showLabels: true
      showValues: true
      unpinAxis: false
      tickDensity: default
      tickDensityCustom: 5
      type: linear
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    colors:
    - 'palette: Looker Classic'
    hidden_fields: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    row: 11
    col: 8
    width: 8
    height: 7
  - name: Top 10 Album Artists by Stream Duration
    title: Top 10 Album Artists by Stream Duration
    model: 
    explore: streaming
    type: looker_pie
    fields:
    - streaming.album_artist_name
    - streaming.stream_duration_hours_sum
    filters: {}
    sorts:
    - streaming.stream_duration_hours_sum desc
    limit: 10
    column_limit: 50
    query_timezone: Europe/London
    value_labels: labels
    label_type: labPer
    stacking: normal
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: true
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    show_null_points: true
    point_style: circle_outline
    interpolation: linear
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    custom_color_enabled: false
    custom_color: forestgreen
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    ordering: none
    show_null_labels: false
    leftAxisLabelVisible: false
    leftAxisLabel: ''
    rightAxisLabelVisible: false
    rightAxisLabel: ''
    barColors:
    - red
    - blue
    smoothedBars: false
    orientation: automatic
    labelPosition: left
    percentType: total
    percentPosition: inline
    valuePosition: right
    labelColorEnabled: false
    labelColor: "#FFF"
    font_size: '12'
    series_types: {}
    hidden_series:
    - streaming.stream_duration_sum
    hide_legend: false
    series_colors: {}
    y_axes:
    - label: Royalty Amount Sum
      orientation: left
      series:
      - id: streaming.royaltyamount_sum
        name: Stage Streaming Royaltyamount Sum
        axisId: streaming.royaltyamount_sum
      - id: streaming.stream_count
        name: Stage Streaming Stream Count
        axisId: streaming.stream_count
      - id: streaming.stream_duration_sum
        name: Stage Streaming Stream Duration Sum
        axisId: streaming.stream_duration_sum
      - id: streaming.distinct_customers
        name: Stage Streaming Distinct Customers
        axisId: streaming.distinct_customers
      showLabels: true
      showValues: true
      unpinAxis: false
      tickDensity: default
      tickDensityCustom: 5
      type: linear
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    colors:
    - 'palette: Looker Classic'
    conditional_formatting:
    - type: low to high
      value:
      background_color:
      font_color:
      palette:
        name: Red to Yellow to Green
        colors:
        - "#F36254"
        - "#FCF758"
        - "#4FBC89"
      bold: false
      italic: false
      strikethrough: false
      fields:
    value_format: "#,##0  €"
    hidden_fields: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    row: 11
    col: 16
    width: 8
    height: 7
  - name: Number of Hours Streamed and Customers Over Time
    title: Number of Hours Streamed and Customers Over Time
    model: 
    explore: streaming
    type: looker_area
    fields:
    - streaming.stream_duration_hours_sum
    - streaming.stream_date
    - streaming.distinct_customers
    fill_fields:
    - streaming.stream_date
    filters: {}
    sorts:
    - streaming.stream_duration_hours_sum desc
    limit: 10
    column_limit: 50
    query_timezone: Europe/London
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: true
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    show_null_points: true
    point_style: circle
    interpolation: linear
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    ordering: none
    show_null_labels: false
    value_labels: labels
    label_type: labPer
    map_plot_mode: points
    heatmap_gridlines: false
    heatmap_gridlines_empty: false
    heatmap_opacity: 0.5
    show_region_field: true
    draw_map_labels_above_data: true
    map_tile_provider: light
    map_position: custom
    map_scale_indicator: 'off'
    map_pannable: true
    map_zoomable: true
    map_marker_type: circle
    map_marker_icon_name: default
    map_marker_radius_mode: proportional_value
    map_marker_units: meters
    map_marker_proportional_scale_type: linear
    map_marker_color_mode: fixed
    show_legend: true
    quantize_map_value_colors: false
    reverse_map_value_colors: false
    custom_color_enabled: false
    custom_color: forestgreen
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    leftAxisLabelVisible: false
    leftAxisLabel: ''
    rightAxisLabelVisible: false
    rightAxisLabel: ''
    barColors:
    - red
    - blue
    smoothedBars: false
    orientation: automatic
    labelPosition: left
    percentType: total
    percentPosition: inline
    valuePosition: right
    labelColorEnabled: false
    labelColor: "#FFF"
    font_size: '12'
    series_types:
      streaming.distinct_customers: line
    hidden_series:
    - streaming.stream_duration_sum
    hide_legend: false
    series_colors: {}
    y_axes:
    - label: Total streaming time (hrs)
      orientation: left
      series:
      - id: streaming.stream_duration_hours_sum
        name: Stage Streaming Stream Duration Hours Sum
        axisId: streaming.stream_duration_hours_sum
      showLabels: true
      showValues: true
      unpinAxis: false
      tickDensity: default
      type: linear
    - label: Number of Distinct Customers
      orientation: right
      series:
      - id: streaming.distinct_customers
        name: Stage Streaming Distinct Customers
        axisId: streaming.distinct_customers
      showLabels: true
      showValues: true
      unpinAxis: false
      tickDensity: default
      type: linear
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    colors:
    - 'palette: Looker Classic'
    conditional_formatting:
    - type: low to high
      value:
      background_color:
      font_color:
      palette:
        name: Red to Yellow to Green
        colors:
        - "#F36254"
        - "#FCF758"
        - "#4FBC89"
      bold: false
      italic: false
      strikethrough: false
      fields:
    value_format: "#,##0  €"
    map_latitude: 51.998410382390325
    map_longitude: 10.502929687500002
    map_zoom: 4
    label_value_format: ''
    inner_radius: 55
    column_group_spacing_ratio:
    swap_axes: false
    focus_on_hover: true
    series_labels:
      streaming.stream_duration_hours_sum: Total Stream Duration
      streaming.distinct_customers: Number of Distinct Customers
    hidden_fields: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    row: 18
    col: 0
    width: 13
    height: 10
  - name: Stream Duration by Customer Age
    title: Stream Duration by Customer Age
    model: 
    explore: streaming
    type: looker_area
    fields:
    - streaming.stream_duration_hours_sum
    - clients.age
    filters: {}
    sorts:
    - streaming.stream_duration_hours_sum desc
    limit: 500
    query_timezone: Europe/London
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: true
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    show_null_points: true
    point_style: none
    interpolation: linear
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    y_axes:
    - label: Stream Duration (hrs)
      orientation: left
      series:
      - id: streaming.stream_duration_hours_sum
        name: Stage Streaming Stream Duration Hours Sum
        axisId: streaming.stream_duration_hours_sum
      showLabels: true
      showValues: true
      unpinAxis: false
      tickDensity: default
      tickDensityCustom: 5
      type: linear
    x_axis_label: Customer Age
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    note_state: collapsed
    note_display: above
    note_text: Only represents customers who have given their age
    row: 18
    col: 13
    width: 11
    height: 10
  - title: streaming_top_songs
    name: streaming_top_songs
    model: 
    explore: streaming
    type: looker_bar
    fields:
    - streaming.track_title
    - streaming.album_artist_name
    - streaming.royalty_amount_sum
    - streaming.stream_count
    - streaming.stream_duration_sum
    - streaming.distinct_customers
    filters: {}
    sorts:
    - streaming.royalty_amount_sum desc
    limit: 10
    query_timezone: Europe/London
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: false
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    show_null_points: true
    point_style: circle_outline
    interpolation: linear
    value_labels: legend
    label_type: labPer
    leftAxisLabelVisible: false
    leftAxisLabel: ''
    rightAxisLabelVisible: false
    rightAxisLabel: ''
    barColors:
    - red
    - blue
    smoothedBars: false
    orientation: automatic
    labelPosition: left
    percentType: total
    percentPosition: inline
    valuePosition: right
    labelColorEnabled: false
    labelColor: "#FFF"
    font_size: '12'
    custom_color_enabled: false
    custom_color: forestgreen
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    hidden_series:
    - streaming.stream_duration_sum
    hide_legend: false
    series_colors:
      streaming.stream_count: "#a9c574"
      streaming.royalty_amount_sum: "#ffb14d"
    y_axes:
    - label: Royalty Amount Sum
      orientation: left
      series:
      - id: streaming.royalty_amount_sum
        name: Stage Streaming Royalty Amount Sum
        axisId: streaming.royalty_amount_sum
      - id: streaming.stream_count
        name: Stage Streaming Stream Count
        axisId: streaming.stream_count
      - id: streaming.stream_duration_sum
        name: Stage Streaming Stream Duration Sum
        axisId: streaming.stream_duration_sum
      - id: streaming.distinct_customers
        name: Stage Streaming Distinct Customers
        axisId: streaming.distinct_customers
      showLabels: true
      showValues: true
      unpinAxis: false
      tickDensity: default
      tickDensityCustom: 5
      type: linear
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    series_labels:
      streaming.royalty_amount_sum: Royalty Amount Sum
    hidden_fields: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    note_state: collapsed
    note_display: above
    note_text: Top 10 Songs by Royalties
    title_hidden: true
    row: 28
    col: 0
    width: 24
    height: 10
  - title: streaming_top_songs_table
    name: streaming_top_songs_table
    model: 
    explore: streaming
    type: table
    fields:
    - streaming.track_title
    - streaming.album_artist_name
    - streaming.album_title
    - streaming.label
    - streaming.supplier
    - streaming.royalty_amount_sum
    - streaming.stream_count
    - streaming.stream_duration_sum
    - streaming.distinct_customers
    filters: {}
    sorts:
    - streaming.royalty_amount_sum desc
    limit: 10
    query_timezone: Europe/London
    show_view_names: true
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    limit_displayed_rows: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    stacking: normal
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    show_null_points: true
    point_style: circle_outline
    leftAxisLabelVisible: false
    leftAxisLabel: ''
    rightAxisLabelVisible: false
    rightAxisLabel: ''
    barColors:
    - red
    - blue
    smoothedBars: false
    orientation: automatic
    labelPosition: left
    percentType: total
    percentPosition: inline
    valuePosition: right
    labelColorEnabled: false
    labelColor: "#FFF"
    font_size: '12'
    custom_color_enabled: false
    custom_color: forestgreen
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    interpolation: linear
    value_labels: legend
    label_type: labPer
    series_types: {}
    hidden_series:
    - streaming.stream_count
    - streaming.distinct_customers
    - streaming.stream_duration_sum
    hide_legend: false
    series_colors:
      streaming.stream_count: "#62bad4"
      streaming.royalty_amount_sum: "#ffb14d"
    y_axes:
    - label: Royalty Amount Sum
      orientation: left
      series:
      - id: streaming.royalty_amount_sum
        name: Stage Streaming Royalty Amount Sum
        axisId: streaming.royalty_amount_sum
      - id: streaming.stream_count
        name: Stage Streaming Stream Count
        axisId: streaming.stream_count
      - id: streaming.distinct_customers
        name: Stage Streaming Distinct Customers
        axisId: streaming.distinct_customers
      showLabels: true
      showValues: true
      unpinAxis: true
      tickDensity: default
      tickDensityCustom: 5
      type: linear
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    series_labels:
      streaming.stream_count: Stream Count
      streaming.stream_duration_sum: Stream Duration Sum
      streaming.distinct_customers: Distinct Customers
      streaming.label: Label
      streaming.supplier: Supplier
      streaming.album_artist_name: Album Artist Name
      streaming.royalty_amount_sum: Royalty Amount Sum
      streaming.track_title: Track Title
      streaming.album_title: Album Title
    hidden_fields: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    note_state: collapsed
    note_display: above
    note_text: Top 10 Songs
    title_hidden: true
    row: 38
    col: 0
    width: 24
    height: 7
  - title: streaming_top_artists
    name: streaming_top_artists
    model: 
    explore: streaming
    type: looker_bar
    fields:
    - streaming.album_artist_name
    - streaming.royalty_amount_sum
    - streaming.stream_count
    - streaming.stream_duration_sum
    - streaming.distinct_customers
    sorts:
    - streaming.royalty_amount_sum desc
    limit: 10
    query_timezone: Europe/London
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: false
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    value_labels: legend
    label_type: labPer
    show_null_points: true
    point_style: circle
    interpolation: linear
    series_types: {}
    hide_legend: false
    hidden_series:
    - streaming.stream_duration_sum
    series_colors:
      streaming.royalty_amount_sum: "#ffb14d"
    series_labels:
      streaming.royalty_amount_sum: Royalty Amount Sum
    y_axes:
    - label: ''
      orientation: bottom
      series:
      - id: streaming.royalty_amount_sum
        name: Royalty Amount Sum
        axisId: streaming.royalty_amount_sum
      - id: streaming.stream_count
        name: Stream Count
        axisId: streaming.stream_count
      - id: streaming.stream_duration_sum
        name: Stream Duration Sum
        axisId: streaming.stream_duration_sum
      - id: streaming.distinct_customers
        name: Distinct Customers
        axisId: streaming.distinct_customers
      showLabels: true
      showValues: true
      unpinAxis: false
      tickDensity: default
      tickDensityCustom: 5
      type: linear
    x_axis_label: Album Artist Name
    hidden_fields: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    note_state: collapsed
    note_display: above
    note_text: Top 10 Artists by Royalties
    title_hidden: true
    row: 45
    col: 0
    width: 24
    height: 9
  - title: streaming_top_artists_table
    name: streaming_top_artists_table
    model: 
    explore: streaming
    type: table
    fields:
    - streaming.album_artist_name
    - streaming.royalty_amount_sum
    - streaming.stream_count
    - streaming.stream_duration_sum
    - streaming.distinct_customers
    sorts:
    - streaming.royalty_amount_sum desc
    limit: 10
    query_timezone: Europe/London
    show_view_names: true
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    limit_displayed_rows: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    stacking: normal
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    value_labels: legend
    label_type: labPer
    show_null_points: true
    point_style: circle
    interpolation: linear
    series_types: {}
    hide_legend: false
    hidden_series:
    - streaming.stream_duration_sum
    series_colors:
      streaming.royalty_amount_sum: "#ffb14d"
    series_labels:
      streaming.stream_count: Stream Count
      streaming.stream_duration_sum: Stream Duration Sum
      streaming.distinct_customers: Distinct Customers
      streaming.album_artist_name: Album Artist Name
      streaming.royalty_amount_sum: Royalty Amount Sum
    hidden_fields: []
    y_axes: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    note_state: collapsed
    note_display: above
    note_text: Top 10 Artists
    title_hidden: true
    row: 54
    col: 0
    width: 24
    height: 7
  - title: streaming_top_labels
    name: streaming_top_labels
    model: 
    explore: streaming
    type: looker_bar
    fields:
    - streaming.label
    - streaming.royalty_amount_sum
    - streaming.stream_count
    - streaming.stream_duration_sum
    - streaming.distinct_customers
    filters: {}
    sorts:
    - streaming.royalty_amount_sum desc
    limit: 10
    query_timezone: Europe/London
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: false
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    series_colors:
      streaming.royalty_amount_sum: "#ffb14d"
    hidden_series:
    - streaming.stream_duration_sum
    series_labels:
      streaming.royalty_amount_sum: Royalty Amount Sum
    hidden_fields: []
    y_axes: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    note_state: collapsed
    note_display: above
    note_text: Top 10 Labels by Royalties
    title_hidden: true
    row: 61
    col: 0
    width: 24
    height: 8
  - title: streaming_top_suppliers
    name: streaming_top_suppliers
    model: 
    explore: streaming
    type: looker_bar
    fields:
    - streaming.supplier
    - streaming.royalty_amount_sum
    - streaming.stream_count
    - streaming.stream_duration_sum
    - streaming.distinct_customers
    filters: {}
    sorts:
    - streaming.royalty_amount_sum desc
    limit: 10
    query_timezone: Europe/London
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: false
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    series_colors:
      streaming.royalty_amount_sum: "#ffb14d"
    hidden_series:
    - streaming.stream_duration_sum
    series_labels:
      streaming.royalty_amount_sum: Royalty Amount Sum
    hidden_fields: []
    y_axes: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    note_state: collapsed
    note_display: above
    note_text: Top 10 Providers by Royalties
    title_hidden: true
    row: 76
    col: 0
    width: 24
    height: 7
  - title: streaming_top_labels_table
    name: streaming_top_labels_table
    model: 
    explore: streaming
    type: table
    fields:
    - streaming.label
    - streaming.royalty_amount_sum
    - streaming.stream_count
    - streaming.stream_duration_sum
    - streaming.distinct_customers
    filters: {}
    sorts:
    - streaming.royalty_amount_sum desc
    limit: 10
    query_timezone: Europe/London
    show_view_names: true
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    limit_displayed_rows: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    stacking: normal
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    series_colors:
      streaming.royalty_amount_sum: "#ffb14d"
    hidden_series:
    - streaming.stream_duration_sum
    series_labels:
      streaming.label: Label
      streaming.stream_count: Stream Count
      streaming.stream_duration_sum: Stream Duration Sum
      streaming.distinct_customers: Distinct Customers
      streaming.royalty_amount_sum: Royalty Amount Sum
    hidden_fields: []
    y_axes: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    note_state: collapsed
    note_display: above
    note_text: Top 10 Labels
    title_hidden: true
    row: 69
    col: 0
    width: 24
    height: 7
  - title: streaming_top_apps
    name: streaming_top_apps
    model: 
    explore: streaming
    type: looker_bar
    fields:
    - streaming.app_name
    - streaming.royalty_amount_sum
    - streaming.stream_count
    - streaming.stream_duration_sum
    - streaming.distinct_customers
    filters: {}
    sorts:
    - streaming.royalty_amount_sum desc
    limit: 10
    query_timezone: Europe/London
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: false
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    series_colors:
      streaming.royalty_amount_sum: "#ffb14d"
    hidden_series:
    - streaming.stream_duration_sum
    series_labels:
      streaming.royalty_amount_sum: Royalty Amount Sum
    x_axis_label: Application
    hidden_fields: []
    y_axes: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    note_state: collapsed
    note_display: above
    note_text: Top 10 Apps by Royalties
    title_hidden: true
    row: 90
    col: 0
    width: 24
    height: 8
  - title: streaming_top_apps_table
    name: streaming_top_apps_table
    model: 
    explore: streaming
    type: table
    fields:
    - streaming.app_name
    - streaming.royalty_amount_sum
    - streaming.stream_count
    - streaming.stream_duration_sum
    - streaming.distinct_customers
    filters: {}
    sorts:
    - streaming.royalty_amount_sum desc
    limit: 10
    query_timezone: Europe/London
    show_view_names: true
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    limit_displayed_rows: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    stacking: normal
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    series_colors:
      streaming.royalty_amount_sum: "#ffb14d"
    hidden_series:
    - streaming.stream_duration_sum
    series_labels:
      streaming.stream_count: Royalty Amount Sum
      streaming.stream_duration_sum: Stream Duration Sum
      streaming.distinct_customers: Distinct Customers
      streaming.app_name: Application
      streaming.royalty_amount_sum: Application Name
    hidden_fields: []
    y_axes: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    note_state: collapsed
    note_display: above
    note_text: Top 10 Apps
    title_hidden: true
    row: 98
    col: 0
    width: 24
    height: 7
  - title: streaming_top_suppliers_table
    name: streaming_top_suppliers_table
    model: 
    explore: streaming
    type: table
    fields:
    - streaming.supplier
    - streaming.royalty_amount_sum
    - streaming.stream_count
    - streaming.stream_duration_sum
    - streaming.distinct_customers
    filters: {}
    sorts:
    - streaming.royalty_amount_sum desc
    limit: 10
    query_timezone: Europe/London
    show_view_names: true
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    limit_displayed_rows: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    stacking: normal
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    series_colors:
      streaming.royalty_amount_sum: "#ffb14d"
    hidden_series:
    - streaming.stream_duration_sum
    series_labels:
      streaming.supplier: Supplier
      streaming.stream_count: Stream Count
      streaming.stream_duration_sum: Stream Duration Sum
      streaming.distinct_customers: Distinct Customers
      streaming.royalty_amount_sum: Royalty Amount Sum
    hidden_fields: []
    y_axes: []
    listen:
      Period: streaming.stream_date
      Zone: streaming.user_zone
      Offer: streaming.offer
      Try and Buy: streaming.try_and_buy
      Application: streaming.app_name
      Provider: streaming.supplier
      Label: streaming.label
      Customer Creation Date: clients.creation_date
      Customer Gender: clients.gender
      Customer Age: clients.age
    note_state: collapsed
    note_display: above
    note_text: Top 10 Providers
    title_hidden: true
    row: 83
    col: 0
    width: 24
    height: 7
  - title: streaming_top_genres_table
    name: streaming_top_genres_table
    model: 
    explore: streaming
    type: table
    fields:
    - streaming.genre_path
    - streaming.royalty_amount_sum
    - streaming.stream_count
    - streaming.stream_duration_sum
    - streaming.distinct_customers
    filters:
      streaming.stream_date: 7 days
    sorts:
    - streaming.royalty_amount_sum desc
    limit: 10
    query_timezone: Europe/London
    show_view_names: true
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    limit_displayed_rows: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    stacking: normal
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    hidden_series:
    - streaming.stream_duration_sum
    series_colors:
      streaming.royalty_amount_sum: "#ffb14d"
    series_labels:
      streaming.stream_count: Stream Count
      streaming.stream_duration_sum: Stream Duration Sum
      streaming.distinct_customers: Distinct Customers
      streaming.royalty_amount_sum: Royalty Amount Sum
      streaming.genre_path: Genre
    hidden_fields: []
    y_axes: []
    note_state: collapsed
    note_display: above
    note_text: Top 10 Genres
    title_hidden: true
    row: 113
    col: 0
    width: 24
    height: 7
  - title: streaming_top_genres
    name: streaming_top_genres
    model: 
    explore: streaming
    type: looker_bar
    fields:
    - streaming.genre_path
    - streaming.royalty_amount_sum
    - streaming.stream_count
    - streaming.stream_duration_sum
    - streaming.distinct_customers
    filters:
      streaming.stream_date: 7 days
    sorts:
    - streaming.royalty_amount_sum desc
    limit: 10
    query_timezone: Europe/London
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: false
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    hidden_series:
    - streaming.stream_duration_sum
    series_colors:
      streaming.royalty_amount_sum: "#ffb14d"
    series_labels:
      streaming.royalty_amount_sum: Royalty Amount Sum
    x_axis_label: Genre
    hidden_fields: []
    y_axes: []
    note_state: collapsed
    note_display: above
    note_text: Top 10 Genres by Royalties
    title_hidden: true
    row: 105
    col: 0
    width: 24
    height: 8
  filters:
  - name: Period
    title: Period
    type: field_filter
    default_value: 7 days
    allow_multiple_values: true
    required: false
    model: 
    explore: streaming
    listens_to_filters: []
    field: streaming.stream_date
  - name: Zone
    title: Zone
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: 
    explore: streaming
    listens_to_filters: []
    field: streaming.user_zone
  - name: Offer
    title: Offer
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: 
    explore: streaming
    listens_to_filters: []
    field: streaming.offer
  - name: Try and Buy
    title: Try and Buy
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: 
    explore: streaming
    listens_to_filters: []
    field: streaming.try_and_buy
  - name: Application
    title: Application
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: 
    explore: streaming
    listens_to_filters: []
    field: streaming.app_name
  - name: Provider
    title: Provider
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: 
    explore: streaming
    listens_to_filters: []
    field: streaming.supplier
  - name: Label
    title: Label
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: 
    explore: streaming
    listens_to_filters: []
    field: streaming.label
  - name: Customer Creation Date
    title: Customer Creation Date
    type: field_filter
    default_value: 7 days
    allow_multiple_values: true
    required: false
    model: 
    explore: clients
    listens_to_filters: []
    field: clients.creation_date
  - name: Customer Gender
    title: Customer Gender
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: 
    explore: clients
    listens_to_filters: []
    field: clients.gender
  - name: Customer Age
    title: Customer Age
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: 
    explore: clients
    listens_to_filters: []
    field: clients.age