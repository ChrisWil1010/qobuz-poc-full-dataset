view: sales_global_auxiliary {
  # this view is meant to be used for total_sum_for_previous_period
  derived_table: {
    sql:
    SELECT
    orderdate,
    total,
    row_number
    FROM PUBLIC.SALES_GLOBAL_RN
    --WHERE
    --CASE
    --WHEN {% parameter sales_global.date_granularity %} = 'Past Week' THEN
    --(ORDERDATE  >= TO_DATE(DATEADD('day', -13, CURRENT_DATE() )) ) AND ( ORDERDATE < TO_DATE(DATEADD('day', 7, DATEADD('day', -13, CURRENT_DATE() ))) )
    --WHEN {% parameter sales_global.date_granularity %} = 'Past Month' THEN
    --(ORDERDATE  >= TO_DATE(DATEADD('day', -59, CURRENT_DATE() )) ) AND ( ORDERDATE < TO_DATE(DATEADD('day', 30, DATEADD('day', -59, CURRENT_DATE() ))) )
    --WHEN {% parameter sales_global.date_granularity %} = 'Past Quarter' THEN
    --(ORDERDATE  >= TO_DATE(DATEADD('day', -119, CURRENT_DATE() )) ) AND ( ORDERDATE < TO_DATE(DATEADD('day', 60, DATEADD('day', -119, CURRENT_DATE() ))) )
    --WHEN {% parameter sales_global.date_granularity %} = 'Past Year' THEN
    --(ORDERDATE  >= TO_DATE(DATEADD('day', -729, CURRENT_DATE() )) ) AND ( ORDERDATE < TO_DATE(DATEADD('day', 365, DATEADD('day', -729, CURRENT_DATE() ))) )
    --ELSE NULL
    --END

    --GROUP BY 1
    ;;
  }

#       sql:
#     (
#     CASE
#     WHEN {% parameter date_granularity %} = 'Past Week' THEN
#     SELECT SUM(TOTAL) FROM PUBLIC.SALES_GLOBAL WHERE
#     (sales_global.ORDERDATE  >= TO_DATE(DATEADD('day', -13, CURRENT_DATE() )) ) AND ( sales_global.ORDERDATE < TO_DATE(DATEADD('day', 7, DATEADD('day', -13, CURRENT_DATE() ))) )
#     WHEN {% parameter date_granularity %} = 'Past Month' THEN
#     SELECT SUM(TOTAL) FROM PUBLIC.SALES_GLOBAL WHERE
#     (sales_global.ORDERDATE  >= TO_DATE(DATEADD('day', -59, CURRENT_DATE() )) ) AND ( sales_global.ORDERDATE < TO_DATE(DATEADD('day', 30, DATEADD('day', -59, CURRENT_DATE() ))) )
#     WHEN {% parameter date_granularity %} = 'Past Quarter' THEN
#       SELECT SUM(TOTAL) FROM PUBLIC.SALES_GLOBAL WHERE
#     (sales_global.ORDERDATE  >= TO_DATE(DATEADD('day', -119, CURRENT_DATE() )) ) AND ( sales_global.ORDERDATE < TO_DATE(DATEADD('day', 60, DATEADD('day', -119, CURRENT_DATE() ))) )
#   WHEN {% parameter date_granularity %} = 'Past Year' THEN
#       SELECT SUM(TOTAL) FROM PUBLIC.SALES_GLOBAL WHERE
#     (sales_global.ORDERDATE  >= TO_DATE(DATEADD('day', -729, CURRENT_DATE() )) ) AND ( sales_global.ORDERDATE < TO_DATE(DATEADD('day', 365, DATEADD('day', -729, CURRENT_DATE() ))) )
#     ELSE NULL
#     END
#     );;

  dimension: order_date_plain {
    type: string
    #primary_key: yes
    sql: ${TABLE}.ORDERDATE ;;
  }

  dimension_group: order {
    type: time
#     timeframes: [
#       raw,
#       date,
#       week,
#       month,
#       quarter,
#       year
#     ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.ORDERDATE ;;

#     sql:
#         CASE
#     WHEN {% parameter sales_global.date_granularity %} = 'Past Week' THEN
#     (${TABLE}.ORDERDATE  >= TO_DATE(DATEADD('day', -13, CURRENT_DATE() )) ) AND ( ${TABLE}.ORDERDATE < TO_DATE(DATEADD('day', 7, DATEADD('day', -13, CURRENT_DATE() ))) )
#     WHEN {% parameter sales_global.date_granularity %} = 'Past Month' THEN
#     (${TABLE}.ORDERDATE  >= TO_DATE(DATEADD('day', -59, CURRENT_DATE() )) ) AND ( ${TABLE}.ORDERDATE < TO_DATE(DATEADD('day', 30, DATEADD('day', -59, CURRENT_DATE() ))) )
#     WHEN {% parameter sales_global.date_granularity %} = 'Past Quarter' THEN
#     (${TABLE}.ORDERDATE  >= TO_DATE(DATEADD('day', -119, CURRENT_DATE() )) ) AND ( ${TABLE}.ORDERDATE < TO_DATE(DATEADD('day', 60, DATEADD('day', -119, CURRENT_DATE() ))) )
#     WHEN {% parameter sales_global.date_granularity %} = 'Past Year' THEN
#     (${TABLE}.ORDERDATE  >= TO_DATE(DATEADD('day', -729, CURRENT_DATE() )) ) AND ( ${TABLE}.ORDERDATE < TO_DATE(DATEADD('day', 365, DATEADD('day', -729, CURRENT_DATE() ))) )
#     ELSE NULL
#     END
#     ;;

  }

  dimension: row_number {
    type: number
    primary_key: yes
    sql: ${TABLE}.ROW_NUMBER ;;
  }

  dimension: total {
    hidden: yes
    type: number
    sql: ${TABLE}.TOTAL ;;
  }

  measure: total_sum_for_previous_period {
    type: sum
    sql: ${total} ;;
  }

  #added to have a parametrized filter on UI
#   parameter: date_granularity {
#     type: string
#     allowed_value: { value: "Past Week" }
#     allowed_value: { value: "Past Month" }
#     allowed_value: { value: "Past Quarter" }
#     allowed_value: { value: "Past Year" }
#   }

}