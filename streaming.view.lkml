view: streaming {
  sql_table_name: PUBLIC.STREAMING ;;

  dimension: album_artist_name {
    type: string
    sql: ${TABLE}.ALBUMARTISTNAME ;;
  }

  dimension: album_artist_slug {
    type: string
    sql: ${TABLE}.ALBUMARTISTSLUG ;;
  }

  dimension: album_grid {
    type: string
    sql: ${TABLE}.ALBUMGRID ;;
  }

  dimension: album_id {
    hidden: no
    type: string
    sql: ${TABLE}.ALBUMID ;;
  }

  dimension: album_title {
    type: string
    sql: ${TABLE}.ALBUMTITLE ;;
  }

  dimension: app_id {
    hidden: no
    type: string
    sql: ${TABLE}.APPID ;;
  }

  dimension: app_name {
    type: string
    sql: ${TABLE}.APPNAME ;;
  }

  dimension: bi_key {
    type: string
    sql: ${TABLE}.BIKEY ;;
  }

  dimension: channel {
    type: string
    sql: ${TABLE}.CHANNEL ;;
  }

  dimension: file_period {
    type: string
    sql: ${TABLE}.FILEPERIOD ;;
  }

  dimension: file_type {
    type: string
    sql: ${TABLE}.FILETYPE ;;
  }

  dimension: file_version {
    type: string
    sql: ${TABLE}.FILEVERSION ;;
  }

  dimension: genre {
    type: string
    sql: ${TABLE}.GENRE ;;
  }

  dimension: genre_path {
    type: string
    sql: ${TABLE}.GENREPATH ;;
  }

  dimension: isrc {
    type: string
    sql: ${TABLE}.ISRC ;;
  }

  dimension: label {
    type: string
    sql: ${TABLE}.LABEL ;;
  }

  dimension: label_id {
    hidden: no
    type: string
    sql: ${TABLE}.LABELID ;;
  }

  dimension: label_slug {
    type: string
    sql: ${TABLE}.LABELSLUG ;;
  }

  dimension: offer {
    type: string
    sql: ${TABLE}.OFFER ;;
  }

  dimension: periodicity {
    type: string
    sql: ${TABLE}.PERIODICITY ;;
  }

  dimension: quantity {
    type: string
    sql: ${TABLE}.QUANTITY ;;
  }

  dimension: royalty_amount {
    type: number
    sql: ${TABLE}.ROYALTYAMOUNT ;;
  }

  dimension: royalty_amount_eur {
    type: number
    sql: ${TABLE}.ROYALTYAMOUNTEUR ;;
  }

  dimension: royalty_currency {
    type: string
    sql: ${TABLE}.ROYALTYCURRENCY ;;
  }

  dimension: royalty_currency_rate {
    type: number
    sql: ${TABLE}.ROYALTYCURRENCYRATE ;;
  }

  dimension: royalty_per_unit {
    type: number
    sql: ${TABLE}.ROYALTYPERUNIT ;;
  }

  dimension: royalty_per_unit_eur {
    type: number
    sql: ${TABLE}.ROYALTYPERUNITEUR ;;
  }

  dimension: stream_country_code {
    type: string
    sql: ${TABLE}.STREAMCOUNTRYCODE ;;
  }

  dimension_group: stream {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql:
    CASE
    WHEN CONTAINS(STREAMDATE, '-') THEN DATEADD(MONTH, 2, TO_DATE(SUBSTRING(${TABLE}.STREAMDATE, 1, 10), 'YYYY-MM-DD'))
    WHEN CONTAINS(STREAMDATE, '/') THEN DATEADD(MONTH, 2, TO_DATE(SUBSTRING(${TABLE}.STREAMDATE, 1, 10), 'YYYY/MM/DD'))
    END
    ;;
    #sql: ${TABLE}.STREAMDATE ;;
    }

    dimension: stream_duration {
      type: number
      sql:  TO_NUMBER(${TABLE}.STREAMDURATION) ;;
      #type: string
      #sql: ${TABLE}.STREAMDURATION ;;
    }

    dimension: stream_ip_address {
      type: string
      sql: ${TABLE}.STREAMIPADDRESS ;;
    }

    dimension: stream_type {
      type: string
      sql: ${TABLE}.STREAMTYPE ;;
    }

    dimension: supplier {
      type: string
      sql: ${TABLE}.SUPPLIER ;;
    }

    dimension: supplier_id {
      hidden: no
      type: string
      sql: ${TABLE}.SUPPLIERID ;;
    }

    dimension: support_number {
      type: string
      sql: ${TABLE}.SUPPORTNUMBER ;;
    }

    dimension: track_artist_name {
      type: string
      sql: ${TABLE}.TRACKARTISTNAME ;;
    }

    dimension: track_artist_slug {
      type: string
      sql: ${TABLE}.TRACKARTISTSLUG ;;
    }

    dimension: track_duration {
      type: string
      sql: ${TABLE}.TRACKDURATION ;;
    }

    dimension: track_grid {
      type: string
      sql: ${TABLE}.TRACKGRID ;;
    }

    dimension: track_id {
      hidden: no
      type: string
      sql: ${TABLE}.TRACKID ;;
    }

    dimension: track_number {
      type: string
      sql: ${TABLE}.TRACKNUMBER ;;
    }

    dimension: track_title {
      type: string
      sql: ${TABLE}.TRACKTITLE ;;
    }

    dimension: try_and_buy {
      type: string
      sql: ${TABLE}.TRYANDBUY ;;
    }

    dimension: upc {
      type: string
      sql: ${TABLE}.UPC ;;
    }

    dimension: user_id {
      hidden: no
      type: string
      sql: ${TABLE}.USERID ;;
    }

    dimension: user_zone {
      map_layer_name: countries
      sql:
        CASE
        WHEN ${TABLE}.USERZONE = 'AF' THEN 'AFG'
        WHEN ${TABLE}.USERZONE = 'AX' THEN 'ALA'
        WHEN ${TABLE}.USERZONE = 'AL' THEN 'ALB'
        WHEN ${TABLE}.USERZONE = 'DZ' THEN 'DZA'
        WHEN ${TABLE}.USERZONE = 'AS' THEN 'ASM'
        WHEN ${TABLE}.USERZONE = 'AD' THEN 'AND'
        WHEN ${TABLE}.USERZONE = 'AO' THEN 'AGO'
        WHEN ${TABLE}.USERZONE = 'AI' THEN 'AIA'
        WHEN ${TABLE}.USERZONE = 'AQ' THEN 'ATA'
        WHEN ${TABLE}.USERZONE = 'AG' THEN 'ATG'
        WHEN ${TABLE}.USERZONE = 'AR' THEN 'ARG'
        WHEN ${TABLE}.USERZONE = 'AM' THEN 'ARM'
        WHEN ${TABLE}.USERZONE = 'AW' THEN 'ABW'
        WHEN ${TABLE}.USERZONE = 'AU' THEN 'AUS'
        WHEN ${TABLE}.USERZONE = 'AT' THEN 'AUT'
        WHEN ${TABLE}.USERZONE = 'AZ' THEN 'AZE'
        WHEN ${TABLE}.USERZONE = 'BS' THEN 'BHS'
        WHEN ${TABLE}.USERZONE = 'BH' THEN 'BHR'
        WHEN ${TABLE}.USERZONE = 'BD' THEN 'BGD'
        WHEN ${TABLE}.USERZONE = 'BB' THEN 'BRB'
        WHEN ${TABLE}.USERZONE = 'BY' THEN 'BLR'
        WHEN ${TABLE}.USERZONE = 'BE' THEN 'BEL'
        WHEN ${TABLE}.USERZONE = 'BZ' THEN 'BLZ'
        WHEN ${TABLE}.USERZONE = 'BJ' THEN 'BEN'
        WHEN ${TABLE}.USERZONE = 'BM' THEN 'BMU'
        WHEN ${TABLE}.USERZONE = 'BT' THEN 'BTN'
        WHEN ${TABLE}.USERZONE = 'BO' THEN 'BOL'
        WHEN ${TABLE}.USERZONE = 'BQ' THEN 'BES'
        WHEN ${TABLE}.USERZONE = 'BA' THEN 'BIH'
        WHEN ${TABLE}.USERZONE = 'BW' THEN 'BWA'
        WHEN ${TABLE}.USERZONE = 'BV' THEN 'BVT'
        WHEN ${TABLE}.USERZONE = 'BR' THEN 'BRA'
        WHEN ${TABLE}.USERZONE = 'IO' THEN 'IOT'
        WHEN ${TABLE}.USERZONE = 'BN' THEN 'BRN'
        WHEN ${TABLE}.USERZONE = 'BG' THEN 'BGR'
        WHEN ${TABLE}.USERZONE = 'BF' THEN 'BFA'
        WHEN ${TABLE}.USERZONE = 'BI' THEN 'BDI'
        WHEN ${TABLE}.USERZONE = 'KH' THEN 'KHM'
        WHEN ${TABLE}.USERZONE = 'CM' THEN 'CMR'
        WHEN ${TABLE}.USERZONE = 'CA' THEN 'CAN'
        WHEN ${TABLE}.USERZONE = 'CV' THEN 'CPV'
        WHEN ${TABLE}.USERZONE = 'KY' THEN 'CYM'
        WHEN ${TABLE}.USERZONE = 'CF' THEN 'CAF'
        WHEN ${TABLE}.USERZONE = 'TD' THEN 'TCD'
        WHEN ${TABLE}.USERZONE = 'CL' THEN 'CHL'
        WHEN ${TABLE}.USERZONE = 'CN' THEN 'CHN'
        WHEN ${TABLE}.USERZONE = 'CX' THEN 'CXR'
        WHEN ${TABLE}.USERZONE = 'CC' THEN 'CCK'
        WHEN ${TABLE}.USERZONE = 'CO' THEN 'COL'
        WHEN ${TABLE}.USERZONE = 'KM' THEN 'COM'
        WHEN ${TABLE}.USERZONE = 'CG' THEN 'COG'
        WHEN ${TABLE}.USERZONE = 'CD' THEN 'COD'
        WHEN ${TABLE}.USERZONE = 'CK' THEN 'COK'
        WHEN ${TABLE}.USERZONE = 'CR' THEN 'CRI'
        WHEN ${TABLE}.USERZONE = 'CI' THEN 'CIV'
        WHEN ${TABLE}.USERZONE = 'HR' THEN 'HRV'
        WHEN ${TABLE}.USERZONE = 'CU' THEN 'CUB'
        WHEN ${TABLE}.USERZONE = 'CW' THEN 'CUW'
        WHEN ${TABLE}.USERZONE = 'CY' THEN 'CYP'
        WHEN ${TABLE}.USERZONE = 'CZ' THEN 'CZE'
        WHEN ${TABLE}.USERZONE = 'DK' THEN 'DNK'
        WHEN ${TABLE}.USERZONE = 'DJ' THEN 'DJI'
        WHEN ${TABLE}.USERZONE = 'DM' THEN 'DMA'
        WHEN ${TABLE}.USERZONE = 'DO' THEN 'DOM'
        WHEN ${TABLE}.USERZONE = 'EC' THEN 'ECU'
        WHEN ${TABLE}.USERZONE = 'EG' THEN 'EGY'
        WHEN ${TABLE}.USERZONE = 'SV' THEN 'SLV'
        WHEN ${TABLE}.USERZONE = 'GQ' THEN 'GNQ'
        WHEN ${TABLE}.USERZONE = 'ER' THEN 'ERI'
        WHEN ${TABLE}.USERZONE = 'EE' THEN 'EST'
        WHEN ${TABLE}.USERZONE = 'ET' THEN 'ETH'
        WHEN ${TABLE}.USERZONE = 'FK' THEN 'FLK'
        WHEN ${TABLE}.USERZONE = 'FO' THEN 'FRO'
        WHEN ${TABLE}.USERZONE = 'FJ' THEN 'FJI'
        WHEN ${TABLE}.USERZONE = 'FI' THEN 'FIN'
        WHEN ${TABLE}.USERZONE = 'FR' THEN 'FRA'
        WHEN ${TABLE}.USERZONE = 'GF' THEN 'GUF'
        WHEN ${TABLE}.USERZONE = 'PF' THEN 'PYF'
        WHEN ${TABLE}.USERZONE = 'TF' THEN 'ATF'
        WHEN ${TABLE}.USERZONE = 'GA' THEN 'GAB'
        WHEN ${TABLE}.USERZONE = 'GM' THEN 'GMB'
        WHEN ${TABLE}.USERZONE = 'GE' THEN 'GEO'
        WHEN ${TABLE}.USERZONE = 'DE' THEN 'DEU'
        WHEN ${TABLE}.USERZONE = 'GH' THEN 'GHA'
        WHEN ${TABLE}.USERZONE = 'GI' THEN 'GIB'
        WHEN ${TABLE}.USERZONE = 'GR' THEN 'GRC'
        WHEN ${TABLE}.USERZONE = 'GL' THEN 'GRL'
        WHEN ${TABLE}.USERZONE = 'GD' THEN 'GRD'
        WHEN ${TABLE}.USERZONE = 'GP' THEN 'GLP'
        WHEN ${TABLE}.USERZONE = 'GU' THEN 'GUM'
        WHEN ${TABLE}.USERZONE = 'GT' THEN 'GTM'
        WHEN ${TABLE}.USERZONE = 'GG' THEN 'GGY'
        WHEN ${TABLE}.USERZONE = 'GN' THEN 'GIN'
        WHEN ${TABLE}.USERZONE = 'GW' THEN 'GNB'
        WHEN ${TABLE}.USERZONE = 'GY' THEN 'GUY'
        WHEN ${TABLE}.USERZONE = 'HT' THEN 'HTI'
        WHEN ${TABLE}.USERZONE = 'HM' THEN 'HMD'
        WHEN ${TABLE}.USERZONE = 'VA' THEN 'VAT'
        WHEN ${TABLE}.USERZONE = 'HN' THEN 'HND'
        WHEN ${TABLE}.USERZONE = 'HK' THEN 'HKG'
        WHEN ${TABLE}.USERZONE = 'HU' THEN 'HUN'
        WHEN ${TABLE}.USERZONE = 'IS' THEN 'ISL'
        WHEN ${TABLE}.USERZONE = 'IN' THEN 'IND'
        WHEN ${TABLE}.USERZONE = 'ID' THEN 'IDN'
        WHEN ${TABLE}.USERZONE = 'IR' THEN 'IRN'
        WHEN ${TABLE}.USERZONE = 'IQ' THEN 'IRQ'
        WHEN ${TABLE}.USERZONE = 'IE' THEN 'IRL'
        WHEN ${TABLE}.USERZONE = 'IM' THEN 'IMN'
        WHEN ${TABLE}.USERZONE = 'IL' THEN 'ISR'
        WHEN ${TABLE}.USERZONE = 'IT' THEN 'ITA'
        WHEN ${TABLE}.USERZONE = 'JM' THEN 'JAM'
        WHEN ${TABLE}.USERZONE = 'JP' THEN 'JPN'
        WHEN ${TABLE}.USERZONE = 'JE' THEN 'JEY'
        WHEN ${TABLE}.USERZONE = 'JO' THEN 'JOR'
        WHEN ${TABLE}.USERZONE = 'KZ' THEN 'KAZ'
        WHEN ${TABLE}.USERZONE = 'KE' THEN 'KEN'
        WHEN ${TABLE}.USERZONE = 'KI' THEN 'KIR'
        WHEN ${TABLE}.USERZONE = 'KP' THEN 'PRK'
        WHEN ${TABLE}.USERZONE = 'KR' THEN 'KOR'
        WHEN ${TABLE}.USERZONE = 'KW' THEN 'KWT'
        WHEN ${TABLE}.USERZONE = 'KG' THEN 'KGZ'
        WHEN ${TABLE}.USERZONE = 'LA' THEN 'LAO'
        WHEN ${TABLE}.USERZONE = 'LV' THEN 'LVA'
        WHEN ${TABLE}.USERZONE = 'LB' THEN 'LBN'
        WHEN ${TABLE}.USERZONE = 'LS' THEN 'LSO'
        WHEN ${TABLE}.USERZONE = 'LR' THEN 'LBR'
        WHEN ${TABLE}.USERZONE = 'LY' THEN 'LBY'
        WHEN ${TABLE}.USERZONE = 'LI' THEN 'LIE'
        WHEN ${TABLE}.USERZONE = 'LT' THEN 'LTU'
        WHEN ${TABLE}.USERZONE = 'LU' THEN 'LUX'
        WHEN ${TABLE}.USERZONE = 'MO' THEN 'MAC'
        WHEN ${TABLE}.USERZONE = 'MK' THEN 'MKD'
        WHEN ${TABLE}.USERZONE = 'MG' THEN 'MDG'
        WHEN ${TABLE}.USERZONE = 'MW' THEN 'MWI'
        WHEN ${TABLE}.USERZONE = 'MY' THEN 'MYS'
        WHEN ${TABLE}.USERZONE = 'MV' THEN 'MDV'
        WHEN ${TABLE}.USERZONE = 'ML' THEN 'MLI'
        WHEN ${TABLE}.USERZONE = 'MT' THEN 'MLT'
        WHEN ${TABLE}.USERZONE = 'MH' THEN 'MHL'
        WHEN ${TABLE}.USERZONE = 'MQ' THEN 'MTQ'
        WHEN ${TABLE}.USERZONE = 'MR' THEN 'MRT'
        WHEN ${TABLE}.USERZONE = 'MU' THEN 'MUS'
        WHEN ${TABLE}.USERZONE = 'YT' THEN 'MYT'
        WHEN ${TABLE}.USERZONE = 'MX' THEN 'MEX'
        WHEN ${TABLE}.USERZONE = 'FM' THEN 'FSM'
        WHEN ${TABLE}.USERZONE = 'MD' THEN 'MDA'
        WHEN ${TABLE}.USERZONE = 'MC' THEN 'MCO'
        WHEN ${TABLE}.USERZONE = 'MN' THEN 'MNG'
        WHEN ${TABLE}.USERZONE = 'ME' THEN 'MNE'
        WHEN ${TABLE}.USERZONE = 'MS' THEN 'MSR'
        WHEN ${TABLE}.USERZONE = 'MA' THEN 'MAR'
        WHEN ${TABLE}.USERZONE = 'MZ' THEN 'MOZ'
        WHEN ${TABLE}.USERZONE = 'MM' THEN 'MMR'
        WHEN ${TABLE}.USERZONE = 'NA' THEN 'NAM'
        WHEN ${TABLE}.USERZONE = 'NR' THEN 'NRU'
        WHEN ${TABLE}.USERZONE = 'NP' THEN 'NPL'
        WHEN ${TABLE}.USERZONE = 'NL' THEN 'NLD'
        WHEN ${TABLE}.USERZONE = 'NC' THEN 'NCL'
        WHEN ${TABLE}.USERZONE = 'NZ' THEN 'NZL'
        WHEN ${TABLE}.USERZONE = 'NI' THEN 'NIC'
        WHEN ${TABLE}.USERZONE = 'NE' THEN 'NER'
        WHEN ${TABLE}.USERZONE = 'NG' THEN 'NGA'
        WHEN ${TABLE}.USERZONE = 'NU' THEN 'NIU'
        WHEN ${TABLE}.USERZONE = 'NF' THEN 'NFK'
        WHEN ${TABLE}.USERZONE = 'MP' THEN 'MNP'
        WHEN ${TABLE}.USERZONE = 'NO' THEN 'NOR'
        WHEN ${TABLE}.USERZONE = 'OM' THEN 'OMN'
        WHEN ${TABLE}.USERZONE = 'PK' THEN 'PAK'
        WHEN ${TABLE}.USERZONE = 'PW' THEN 'PLW'
        WHEN ${TABLE}.USERZONE = 'PS' THEN 'PSE'
        WHEN ${TABLE}.USERZONE = 'PA' THEN 'PAN'
        WHEN ${TABLE}.USERZONE = 'PG' THEN 'PNG'
        WHEN ${TABLE}.USERZONE = 'PY' THEN 'PRY'
        WHEN ${TABLE}.USERZONE = 'PE' THEN 'PER'
        WHEN ${TABLE}.USERZONE = 'PH' THEN 'PHL'
        WHEN ${TABLE}.USERZONE = 'PN' THEN 'PCN'
        WHEN ${TABLE}.USERZONE = 'PL' THEN 'POL'
        WHEN ${TABLE}.USERZONE = 'PT' THEN 'PRT'
        WHEN ${TABLE}.USERZONE = 'PR' THEN 'PRI'
        WHEN ${TABLE}.USERZONE = 'QA' THEN 'QAT'
        WHEN ${TABLE}.USERZONE = 'RE' THEN 'REU'
        WHEN ${TABLE}.USERZONE = 'RO' THEN 'ROU'
        WHEN ${TABLE}.USERZONE = 'RU' THEN 'RUS'
        WHEN ${TABLE}.USERZONE = 'RW' THEN 'RWA'
        WHEN ${TABLE}.USERZONE = 'BL' THEN 'BLM'
        WHEN ${TABLE}.USERZONE = 'SH' THEN 'SHN'
        WHEN ${TABLE}.USERZONE = 'KN' THEN 'KNA'
        WHEN ${TABLE}.USERZONE = 'LC' THEN 'LCA'
        WHEN ${TABLE}.USERZONE = 'MF' THEN 'MAF'
        WHEN ${TABLE}.USERZONE = 'PM' THEN 'SPM'
        WHEN ${TABLE}.USERZONE = 'VC' THEN 'VCT'
        WHEN ${TABLE}.USERZONE = 'WS' THEN 'WSM'
        WHEN ${TABLE}.USERZONE = 'SM' THEN 'SMR'
        WHEN ${TABLE}.USERZONE = 'ST' THEN 'STP'
        WHEN ${TABLE}.USERZONE = 'SA' THEN 'SAU'
        WHEN ${TABLE}.USERZONE = 'SN' THEN 'SEN'
        WHEN ${TABLE}.USERZONE = 'RS' THEN 'SRB'
        WHEN ${TABLE}.USERZONE = 'SC' THEN 'SYC'
        WHEN ${TABLE}.USERZONE = 'SL' THEN 'SLE'
        WHEN ${TABLE}.USERZONE = 'SG' THEN 'SGP'
        WHEN ${TABLE}.USERZONE = 'SX' THEN 'SXM'
        WHEN ${TABLE}.USERZONE = 'SK' THEN 'SVK'
        WHEN ${TABLE}.USERZONE = 'SI' THEN 'SVN'
        WHEN ${TABLE}.USERZONE = 'SB' THEN 'SLB'
        WHEN ${TABLE}.USERZONE = 'SO' THEN 'SOM'
        WHEN ${TABLE}.USERZONE = 'ZA' THEN 'ZAF'
        WHEN ${TABLE}.USERZONE = 'GS' THEN 'SGS'
        WHEN ${TABLE}.USERZONE = 'SS' THEN 'SSD'
        WHEN ${TABLE}.USERZONE = 'ES' THEN 'ESP'
        WHEN ${TABLE}.USERZONE = 'LK' THEN 'LKA'
        WHEN ${TABLE}.USERZONE = 'SD' THEN 'SDN'
        WHEN ${TABLE}.USERZONE = 'SR' THEN 'SUR'
        WHEN ${TABLE}.USERZONE = 'SJ' THEN 'SJM'
        WHEN ${TABLE}.USERZONE = 'SZ' THEN 'SWZ'
        WHEN ${TABLE}.USERZONE = 'SE' THEN 'SWE'
        WHEN ${TABLE}.USERZONE = 'CH' THEN 'CHE'
        WHEN ${TABLE}.USERZONE = 'SY' THEN 'SYR'
        WHEN ${TABLE}.USERZONE = 'TW' THEN 'TWN'
        WHEN ${TABLE}.USERZONE = 'TJ' THEN 'TJK'
        WHEN ${TABLE}.USERZONE = 'TZ' THEN 'TZA'
        WHEN ${TABLE}.USERZONE = 'TH' THEN 'THA'
        WHEN ${TABLE}.USERZONE = 'TL' THEN 'TLS'
        WHEN ${TABLE}.USERZONE = 'TG' THEN 'TGO'
        WHEN ${TABLE}.USERZONE = 'TK' THEN 'TKL'
        WHEN ${TABLE}.USERZONE = 'TO' THEN 'TON'
        WHEN ${TABLE}.USERZONE = 'TT' THEN 'TTO'
        WHEN ${TABLE}.USERZONE = 'TN' THEN 'TUN'
        WHEN ${TABLE}.USERZONE = 'TR' THEN 'TUR'
        WHEN ${TABLE}.USERZONE = 'TM' THEN 'TKM'
        WHEN ${TABLE}.USERZONE = 'TC' THEN 'TCA'
        WHEN ${TABLE}.USERZONE = 'TV' THEN 'TUV'
        WHEN ${TABLE}.USERZONE = 'UG' THEN 'UGA'
        WHEN ${TABLE}.USERZONE = 'UA' THEN 'UKR'
        WHEN ${TABLE}.USERZONE = 'AE' THEN 'ARE'
        WHEN ${TABLE}.USERZONE = 'GB' THEN 'GBR'
        WHEN ${TABLE}.USERZONE = 'US' THEN 'USA'
        WHEN ${TABLE}.USERZONE = 'UM' THEN 'UMI'
        WHEN ${TABLE}.USERZONE = 'UY' THEN 'URY'
        WHEN ${TABLE}.USERZONE = 'UZ' THEN 'UZB'
        WHEN ${TABLE}.USERZONE = 'VU' THEN 'VUT'
        WHEN ${TABLE}.USERZONE = 'VE' THEN 'VEN'
        WHEN ${TABLE}.USERZONE = 'VN' THEN 'VNM'
        WHEN ${TABLE}.USERZONE = 'VG' THEN 'VGB'
        WHEN ${TABLE}.USERZONE = 'VI' THEN 'VIR'
        WHEN ${TABLE}.USERZONE = 'WF' THEN 'WLF'
        WHEN ${TABLE}.USERZONE = 'EH' THEN 'ESH'
        WHEN ${TABLE}.USERZONE = 'YE' THEN 'YEM'
        WHEN ${TABLE}.USERZONE = 'ZM' THEN 'ZMB'
        WHEN ${TABLE}.USERZONE = 'ZW' THEN 'ZWE'
      ELSE NULL
      END ;;
    }

    measure: count {
      type: count
      drill_fields: [app_name, album_artist_name, track_artist_name]
    }

    measure: royalty_amount_sum {
      type: sum
      sql: ${royalty_amount} ;;
      value_format_name: decimal_2
    }

    measure: stream_count {
      type: count
      #type: count_distinct
      # that's not actually a correct definition, but for now there is no other way to find out Stream Counts
      #sql: ${streamduration} ;;
      drill_fields: [stream_duration]
    }

    measure: distinct_customers {
      type: count_distinct
      #type: sum_distinct
      #sql_distinct_key: ${TABLE}.USERID ;;
      sql: ${user_id} ;;
    }

    measure: stream_duration_sum {
      type: sum
      sql: ${stream_duration} ;;
      #type:  number
      #sql: TO_NUMBER(${TABLE}.STREAMDURATION) ;;
    }

    measure:  stream_duration_hours_sum{
      type: sum
      sql: ${stream_duration} / 3600 ;;
    }

  }
