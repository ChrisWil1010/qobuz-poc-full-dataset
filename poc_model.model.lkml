connection: "snowflake_2yrs_sample_data"

# include all the views
include: "clients.view"
include: "sales_download.view"
include: "sales_global.view"
include: "sales_global_auxiliary.view"
include: "streaming.view"
include: "subscriptions.view"
include: "events.view"

# include all the dashboards
include: "*.dashboard"

datagroup: poc_model_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "24 hour"
}

persist_with: poc_model_default_datagroup

explore: sales_global {}
#   join: sales_global_auxiliary {
#      #type: left_outer
#       type: inner
#        relationship: one_to_one
#     sql_on: ${sales_global.row_number} = ${sales_global_auxiliary.row_number};;
#foreign_key: sales_global.row_number
#   }
# }

explore: clients {
  join: subscriptions {
    type: inner
    relationship: one_to_one
    sql_on: ${clients.id_client} = ${subscriptions.user_id} ;;
  }
  join: sales_global {
    type: inner
    relationship: one_to_one
    sql_on: ${clients.id_client} = ${sales_global.user_id} ;;
  }
}

explore: sales_download {
  join: sales_global {
    type: inner
    relationship: one_to_one
    sql_on: ${sales_download.user_id} = ${sales_global.user_id} ;;
  }
  join: clients {
    type: inner
    relationship: one_to_one
    sql_on: ${sales_download.user_id} = ${clients.id_client} ;;
  }
  join: subscriptions {
    type: inner
    relationship: one_to_one
    sql_on: ${sales_download.user_id} = ${subscriptions.user_id} ;;
  }
}

explore: streaming {
  join: clients {
    #type: left_outer
    type: inner
    relationship: one_to_one
    sql_on: ${streaming.user_id} = ${clients.id_client} ;;
  }
}

explore: subscriptions {
  join: sales_global {
    type: inner
    relationship: one_to_one
    sql_on: ${subscriptions.user_id} = ${sales_global.user_id} ;;
  }
}

explore: events {
  join: subscriptions{
    type: inner
    relationship: one_to_one
    sql_on: ${subscriptions.sub_id} = ${events.event_id} ;;
  }
}
